package com.epam.news.service.impl;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.service.ICommentService;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceTest {

	@Mock
	private ICommentDAO commentDAO;
	
	@InjectMocks
	private ICommentService commentService = new CommentServiceImpl();
	
	@Test
	public void testAdd() throws Exception {
		Comment comment = new Comment();
		comment.setNewsId(1L);
		comment.setText("text");
		comment.setCreationDate(new Date(System.currentTimeMillis()));
		Mockito.when(commentDAO.add(comment)).thenReturn(1L);
		Long result = commentService.add(comment);
		verify(commentDAO).add(comment);
		Assert.assertEquals(1L, (long) result);
	}
	
	@Test
	public void testFindByNewsId() throws Exception {
		List<Comment> comments = new ArrayList<Comment>();
		comments.add(new Comment(1L, "1", new Date(System.currentTimeMillis()), 1L));
		Mockito.when(commentDAO.findByNewsId(1L)).thenReturn(comments);
		List<Comment> result = commentService.findByNewsId(1L);
		verify(commentDAO).findByNewsId(1L);
		Assert.assertEquals(comments, result);
	}
	
	@Test
	public void testRemove() throws Exception {
		commentService.remove(anyLong());
		verify(commentDAO).remove(anyLong());
		verifyNoMoreInteractions(commentDAO);
	}
}
