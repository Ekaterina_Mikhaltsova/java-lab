package com.epam.news.service.impl;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.service.IAuthorService;

@RunWith(MockitoJUnitRunner.class)
public class AuthorServiceTest {

	@Mock
	private IAuthorDAO authorDAO;
	
	@InjectMocks
	private IAuthorService authorService = new AuthorServiceImpl();
	
	@Test
	public void testAdd() throws Exception {
		Author author = new Author();
		author.setName("author");
		Mockito.when(authorDAO.add(author)).thenReturn(1L);
		Long result = authorService.add(author);
		verify(authorDAO).add(author);
		Assert.assertEquals(1L, (long) result);
	}
	
	@Test
	public void testFindByNewsId() throws Exception {
		Author author = new Author(1L, "1", null);
		Mockito.when(authorDAO.findByNewsId(1L)).thenReturn(author);
		Author result = authorService.findByNewsId(1L);
		verify(authorDAO).findByNewsId(1L);
		Assert.assertEquals(author, result);
	}
	
	@Test
	public void testExpired() throws Exception {
		authorService.expired(anyLong());
		verify(authorDAO).expired(anyLong());
		verifyNoMoreInteractions(authorDAO);
	}
}
