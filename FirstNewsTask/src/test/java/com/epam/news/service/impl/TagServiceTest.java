package com.epam.news.service.impl;

import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.news.dao.ITagDAO;
import com.epam.news.service.ITagService;

@RunWith(MockitoJUnitRunner.class)
public class TagServiceTest {

	@Mock
	private ITagDAO tagDAO;
	
	@InjectMocks
	private ITagService tagService = new TagServiceImpl();
	
	@Test
	public void testFindByNewsId() throws Exception {
		tagService.findByNewsId(anyLong());
		verify(tagDAO).findByNewsId(anyLong());
		verifyNoMoreInteractions(tagDAO);
	}
}
