package com.epam.news.dao.impl;

import static org.junit.Assert.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.dao.INewsDAO;
import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;

public class NewsDAOTest extends DAOTest {

	@Autowired
	private INewsDAO newsDAO;	
	
	@Autowired
	private IAuthorDAO authorDAO;
	
	@Autowired
	private ITagDAO tagDAO;
	
	@Test
	public void testCreate() throws Exception {
		News news = new News();
		news.setTitle("title4");
		news.setShortText("short4");
		news.setFullText("full4");
		Assert.assertTrue(newsDAO.add(news) > 0L);
	}

	@Test
	public void testFindById() throws Exception {
		News news = newsDAO.findById(1L);
		assertTrue(news.getId() == 1L);
	}
	
	@Test
	public void testUpdate() throws Exception {
		News news = new News();
		news.setId(7L);
		news.setTitle("newTitle");
		news.setShortText("newShortText");
		news.setFullText("newFullText");
		news.setModificationDate(new Date(System.currentTimeMillis()));
		newsDAO.update(news);
		Assert.assertEquals(newsDAO.findById(7L), news);
	}
	
	@Test
	public void testRemove() throws Exception {
		News news = new News();
		newsDAO.remove(10L);
		News actualNews = newsDAO.findById(10L);
		Assert.assertEquals(news, actualNews);
	}
	
	@Test
	public void testBindTags() throws Exception {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1L, "tag1"));
		tags.add(new Tag(2L, "tag2"));
		newsDAO.bindTags(3L, tags);
		Assert.assertEquals(tagDAO.findByNewsId(3L), tags);
	}
	 
	@Test
	public void testBindAuthor() throws Exception {
		newsDAO.bindAuthor(2L, 2L);
		Assert.assertTrue(authorDAO.findByNewsId(2L).getId() == 2L);
	}
	
	@Test
	public void testSearch() throws Exception {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setAuthorId(1L);
		newsDAO.search(criteria);
	}
}
