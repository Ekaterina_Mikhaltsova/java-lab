package com.epam.news.dao.impl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.service.impl.NewsManagementService;

public class AuthorDAOTest extends DAOTest {

	@Autowired
	private IAuthorDAO authorDAO;
	
	@Autowired
	private NewsManagementService service;
	
	@Test
	public void testTransaction() throws Exception {
		News news = new News();
		news.setTitle("transaction");
		news.setShortText("transaction");
		news.setFullText("transaction");
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(news);
		service.add(newsVO);
	}

	@Test
	public void testAdd() throws Exception {
		Author author = new Author();
		author.setName("author2");
		assertTrue(authorDAO.add(author) > 0L);
	}
	
	@Test
	public void testFindByNewsId() throws Exception {
		Author author = new Author(1L, "1", null);
		assertEquals(authorDAO.findByNewsId(1L), author);
	}
	
	@Test
	public void testExpired() throws Exception {
		authorDAO.expired(1L);
		assertTrue(authorDAO.findByNewsId(1L).getExpired() != null);
	}
}
