package com.epam.news.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;

public class TagDAOTest extends DAOTest {

	@Autowired
	private ITagDAO tagDAO;
	
	@Test
	public void testFindByNewsId() throws Exception {
		List<Tag> tags = new ArrayList<Tag>();
		tags.add(new Tag(1L, "tag1"));
		tags.add(new Tag(2L, "tag2"));
		Assert.assertEquals(tagDAO.findByNewsId(1L), tags);
	}
}
