package com.epam.news.dao.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;

public class CommentDAOTest extends DAOTest {

	@Autowired
	private ICommentDAO commentDAO;

	@Test
	public void testAdd() throws Exception {
		Comment comment = new Comment();
		comment.setText("2");
		comment.setNewsId(1L);
		Assert.assertTrue(commentDAO.add(comment) > 0L);		
	}
	
	@Test
	public void testRemove() throws Exception {
		commentDAO.remove(3L);
	}	
	
	@Test
	public void testFindByNewsId() throws Exception {
		List<Comment> comments = new ArrayList<Comment>();
		DateFormat format = new SimpleDateFormat("EEE MMM dd hh:mm:ss z yyyy");
		Date date1 = format.parse("THU AUG 20 00:00:00 MSK 2015");
		Date date2 = format.parse("MON AUG 31 00:00:00 MSK 2015");
		comments.add(new Comment(1L, "text1", date1, 1L));
		comments.add(new Comment(2L, "2", date2, 1L));
		Assert.assertEquals(commentDAO.findByNewsId(1L), comments);
	}
}
