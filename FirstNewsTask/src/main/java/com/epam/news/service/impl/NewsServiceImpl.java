package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news.dao.INewsDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;
import com.epam.news.service.INewsService;
import com.epam.news.service.exception.ServiceException;

/**
 * Class implementation of {@link INewsService} interface.
 * <p>
 * Implements all operations defined in {@link INewsService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class NewsServiceImpl implements INewsService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	private INewsDAO newsDAO;

	public Long add(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding news", e);
		}
	}

	public void bindTags(Long newsId, List<Tag> tags) throws ServiceException {
		try {
			newsDAO.bindTags(newsId, tags);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding tags to news", e);
		}
	}

	public void bindAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.bindAuthor(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding author to news", e);
		}
	}

	public News findById(Long newsId) throws ServiceException {
		try {
			return newsDAO.findById(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding news by ID", e);
		}
	}

	public void update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while updating news", e);
		}
	}
	
	@Transactional
	public void remove(Long newsId) throws ServiceException {
		try {
			newsDAO.unbindTags(newsId);
			newsDAO.unbindAuthor(newsId);
			newsDAO.remove(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing news", e);
		}
	}

	public List<News> readAll() throws ServiceException {
		try {
			return newsDAO.readAll();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while reading all news", e);
		}
	}

	public int countAll() throws ServiceException {
		try {
			return newsDAO.countAll();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while counting news", e);
		}
	}

	public List<News> search(SearchCriteria searchCriteria)	throws ServiceException {
		// TODO Auto-generated method stub
		return null;
	}

	public INewsDAO getNewsDAO() {
		return newsDAO;
	}

	public void setNewsDAO(INewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
}
