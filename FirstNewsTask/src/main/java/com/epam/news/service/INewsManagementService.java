package com.epam.news.service;

import com.epam.news.entity.NewsVO;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link NewsVO} model.
 * <p>
 * Contains transactional methods for interacting with linked database tables.
 * </p>
 * 
 * @author Kate
 *
 */
public interface INewsManagementService {

	/**
     * Method adds given newsVO.
     * 
     * @param newsVO the newsVO to be added
     * 
     * @return {@link Long} id of created newsVO
     * 
     * @throws ServiceException
     */
	Long add(NewsVO newsVO) throws ServiceException;
	
	/**
     * Method finds newsVO by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link NewsVO} object
     * 
     * @throws ServiceException
     */
	NewsVO findByNewsId(Long newsId) throws ServiceException;
}
