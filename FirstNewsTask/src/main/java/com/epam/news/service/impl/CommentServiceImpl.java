package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Comment;
import com.epam.news.service.ICommentService;
import com.epam.news.service.exception.ServiceException;

/**
 * Class implementation of {@link ICommentService} interface.
 * <p>
 * Implements all operations defined in {@link ICommentService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentServiceImpl implements ICommentService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	private ICommentDAO commentDAO;

	public Long add(Comment comment) throws ServiceException {
		try {
			return commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding comment", e);
		}
	}

	public void remove(Long commentId) throws ServiceException {
		try {
			commentDAO.remove(commentId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing comment", e);
		}
	}

	public List<Comment> findByNewsId(Long newsId) throws ServiceException {
		try {
			return commentDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing comment", e);
		}
	}

	public ICommentDAO getCommentDAO() {
		return commentDAO;
	}

	public void setCommentDAO(ICommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
}
