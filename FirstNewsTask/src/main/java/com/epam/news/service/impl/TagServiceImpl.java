package com.epam.news.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news.dao.ITagDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Tag;
import com.epam.news.service.ITagService;
import com.epam.news.service.exception.ServiceException;

/**
 * Class implementation of {@link ITagService} interface.
 * <p>
 * Implements all operations defined in {@link ITagService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagServiceImpl implements ITagService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	private ITagDAO tagDAO;
	
	public List<Tag> findByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding tags by news ID", e);
		}
	}

	public ITagDAO getTagDAO() {
		return tagDAO;
	}

	public void setTagDAO(ITagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
}
