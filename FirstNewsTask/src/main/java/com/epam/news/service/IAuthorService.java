package com.epam.news.service;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.entity.Author;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Author} model.
 * <p>
 * Contains methods for interaction with {@link IAuthorDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface IAuthorService {
	
	/**
     * Method adds given author.
     * 
     * @param author the author to be added
     * 
     * @return {@link Long} id of created author
     * 
     * @throws ServiceException
     */
	Long add(Author author) throws ServiceException;
	
	/**
     * Method finds author by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @return found {@link Author} author
     * 
     * @throws ServiceException
     */
	Author findByNewsId(Long newsId) throws ServiceException;
	
	/**
     * Method makes given author expired.
     * 
     * @param authorId id of author to be made expired
     * 
     * @throws ServiceException
     */
	void expired(Long authorId) throws ServiceException;
}
