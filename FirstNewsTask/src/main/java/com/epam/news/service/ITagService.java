package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.ITagDAO;
import com.epam.news.entity.Tag;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Tag} model.
 * <p>
 * Contains methods for interaction with {@link ITagDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface ITagService {

	/**
     * Method finds tags by given {@link Long} news id.
     * 
     * @return found {@link List} of {@link Tag} tags
     * 
     * @throws ServiceException
     */
	List<Tag> findByNewsId(Long newsId) throws ServiceException;
}
