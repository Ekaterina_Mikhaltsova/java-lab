package com.epam.news.service.exception;

/**
 * Class defines Service layer exception.
 * <p>
 * This exception is thrown in service methods if any problem with calling 
 * DAO methods occurs.
 * </p>
 * 
 * @author Kate
 *
 */
public class ServiceException extends Exception {
	
	private static final long serialVersionUID = 1L;

	/**
     * Constructor that gets {@link String} message.
     * 
     * @param message defines why the exception was thrown.
     */
	public ServiceException(String message) {
		super(message);
	}
	
	/**
     * Constructor that gets {@link Throwable} cause.
     * 
     * @param cause defines the underlying cause.
     */
	public ServiceException(Throwable cause) {
		super(cause);
	}
	
	/**
     * Constructor that gets {@link String} message and {@link Throwable} cause.
     * 
     * @param message defines why the exception was thrown.
     * @param cause defines the underlying cause.
     */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
}
