package com.epam.news.service.impl;

import org.apache.log4j.Logger;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Author;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.exception.ServiceException;

/**
 * Class implementation of {@link IAuthorService} interface.
 * <p>
 * Implements all operations defined in {@link IAuthorService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorServiceImpl implements IAuthorService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	private IAuthorDAO authorDAO;
	
	public Long add(Author author) throws ServiceException {
		try {
			return authorDAO.add(author);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding author", e);
		}
	}

	public Author findByNewsId(Long newsId) throws ServiceException {
		try {
			return authorDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding author by news ID", e);
		}
	}

	public void expired(Long authorId) throws ServiceException {
		try {
			authorDAO.expired(authorId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while making author expired", e);
		}
	}

	public IAuthorDAO getAuthorDAO() {
		return authorDAO;
	}

	public void setAuthorDAO(IAuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}	
}
