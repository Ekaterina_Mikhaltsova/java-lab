package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.entity.Comment;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Comment} model.
 * <p>
 * Contains methods for interaction with {@link ICommentDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface ICommentService {

	/**
     * Method adds given comment.
     * 
     * @param comment the comment to be added
     * 
     * @return {@link Long} id of created comment
     * 
     * @throws ServiceException
     */
	 Long add(Comment comment) throws ServiceException;
	 
	 /**
	     * Method removes comment by given {@link Long} news id.
	     * 
	     * @param commentId id of comment to be removed
	     * 
	     * @return {@link Long} id of created comment
	     * 
	     * @throws ServiceException
	     */
	 void remove(Long commentId) throws ServiceException;
	 
	 /**
	     * Method finds comments by given {@link Long} news id.
	     * 
	     * @param newsId id of news
	     * 
	     * @return found {@link List} of {@link Comment} comments
	     * 
	     * @throws ServiceException
	     */
	 List<Comment> findByNewsId(Long newsId) throws ServiceException;
}
