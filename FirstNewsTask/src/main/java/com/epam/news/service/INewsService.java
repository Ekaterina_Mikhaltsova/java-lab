package com.epam.news.service;

import java.util.List;

import com.epam.news.dao.INewsDAO;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;
import com.epam.news.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link News} model.
 * <p>
 * Contains methods for interaction with {@link INewsDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface INewsService {

	/**
     * Method adds given news.
     * 
     * @param news the news to be added
     * 
     * @return {@link Long} id of created news
     * 
     * @throws ServiceException
     */
	Long add(News news) throws ServiceException;
	
	/**
     * Method binds tags to news by given {@link Long} news id and {@link List} of {@link Tag} ids.
     * 
     * @param newsId id of news to be bound
     * @param tags list of tags to be bound
     * 
     * @throws ServiceException
     */
	void bindTags(Long newsId, List<Tag> tags) throws ServiceException;
	
	/**
     * Method binds author to news by given {@link Long} news id and {@link Author} author id.
     * 
     * @param newsId id of news to be bound
     * @param authorId id of author to be bound
     * 
     * @throws ServiceException
     */
	void bindAuthor(Long newsId, Long authorId) throws ServiceException;
	
	/**
     * Method finds news by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link News} news
     * 
     * @throws ServiceException
     */
	News findById(Long newsId) throws ServiceException;
	
	/**
     * Method updates news by given {@link News} news.
     * 
     * @param news the news with updated information and same id
     * 
     * @throws ServiceException
     */
	void update(News news) throws ServiceException;
	
	/**
     * Method removes news by given {@link Long} news id.
     * 
     * @param newsId id of news to be removed
     * 
     * @throws ServiceException
     */
	void remove(Long newsId) throws ServiceException;
	
	//to be deleted
	List<News> readAll() throws ServiceException;
	
	/**
     * Method counts all news.
     * 
     * @return news quantity
     * 
     * @throws ServiceException
     */
	int countAll() throws ServiceException;
	
	/**
     * Method finds news according to {@link SearchCriteria} criteria and sorts by most commented news
     * and modification date.
     * 
     * @param searchCriteria criteria of searching for news
     * 
     * @return {@link List} of {@link News} objects
     * 
     * @throws ServiceException
     */
	List<News> search(SearchCriteria searchCriteria) throws ServiceException;
}
