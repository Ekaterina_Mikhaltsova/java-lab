package com.epam.news.service.impl;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news.entity.News;
import com.epam.news.entity.NewsVO;
import com.epam.news.service.IAuthorService;
import com.epam.news.service.ICommentService;
import com.epam.news.service.INewsManagementService;
import com.epam.news.service.INewsService;
import com.epam.news.service.ITagService;
import com.epam.news.service.exception.ServiceException;

/**
 * Class implementation of {@link INewsManagementService} interface.
 * <p>
 * Implements all operations defined in {@link INewsManagementService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
@Transactional(rollbackFor = Exception.class)
public class NewsManagementService implements INewsManagementService {
		
	private INewsService newsService;
		
	private IAuthorService authorService;	
	
	private ITagService tagService;	
	
	private ICommentService commentService;
	
	
	public Long add(NewsVO newsVO) throws ServiceException {
		News news = newsVO.getNews();
		Long newsId = newsService.add(news);
//		throw new ServiceException("Transaction");
		newsService.bindTags(newsId, newsVO.getTags());
		newsService.bindAuthor(newsId, newsVO.getAuthor().getId());
		return newsId;
	}
	
	
	public NewsVO findByNewsId(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsService.findById(newsId));
		newsVO.setAuthor(authorService.findByNewsId(newsId));
		newsVO.setTags(tagService.findByNewsId(newsId));
		newsVO.setComments(commentService.findByNewsId(newsId));
		return newsVO;
	}

	public INewsService getNewsService() {
		return newsService;
	}

	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}

	public IAuthorService getAuthorService() {
		return authorService;
	}

	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}

	public ITagService getTagService() {
		return tagService;
	}

	public void setTagService(ITagService tagService) {
		this.tagService = tagService;
	}

	public ICommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}
}
