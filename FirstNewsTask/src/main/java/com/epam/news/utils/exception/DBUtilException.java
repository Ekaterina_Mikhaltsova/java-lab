package com.epam.news.utils.exception;

/**
 * Class defines DBUtil exception.
 * <p>
 * This exception is thrown in DBUtil methods if any problem with closing
 * connection and other resources.
 * </p>
 * 
 * @author Kate
 *
 */
public class DBUtilException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
     * Constructor that gets {@link String} message.
     * 
     * @param message defines why the exception was thrown.
     */
	public DBUtilException(String message) {
		super(message);
	}
	
	/**
     * Constructor that gets {@link Throwable} cause.
     * 
     * @param cause defines the underlying cause.
     */
	public DBUtilException(Throwable cause) {
		super(cause);
	} 
	
	/**
     * Constructor that gets {@link String} message and {@link Throwable} cause.
     * 
     * @param message defines why the exception was thrown.
     * @param cause defines the underlying cause.
     */
	public DBUtilException(String message, Throwable cause) {
		super(message, cause);
	}
}
