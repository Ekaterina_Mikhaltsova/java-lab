package com.epam.news.dao;

import java.util.List;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.News;
import com.epam.news.entity.SearchCriteria;
import com.epam.news.entity.Tag;

/**
 * Interface that represents a contract for a DAO for the {@link News} model.
 * <p>
 * Contains methods for interaction with database NEWS table and linked tables NEWS_AUTHORS and NEWS_TAGS.
 * </p>
 * 
 * @author Kate
 *
 */
public interface INewsDAO {

	/**
     * Method adds given news to the NEWS table.
     * 
     * @param news the news to be added
     * 
     * @return {@link Long} id of created news
     * 
     * @throws DAOException
     */
	Long add(News news) throws DAOException;
	
	/**
     * Method finds news by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link News} news
     * 
     * @throws DAOException
     */
	News findById(Long newsId) throws DAOException;
	
	/**
     * Method updates news by given {@link News} news.
     * 
     * @param news the news with updated information and same id
     * 
     * @throws DAOException
     */
	void update(News news) throws DAOException;
	
	/**
     * Method removes news by given {@link Long} news id.
     * 
     * @param newsId id of news to be removed
     * 
     * @throws DAOException
     */
	void remove(Long newsId) throws DAOException;
	
	//to be deleted
	List<News> readAll() throws DAOException;
	
	/**
     * Method counts all news.
     * 
     * @return news quantity
     * 
     * @throws DAOException
     */
	int countAll() throws DAOException;
	
	/**
     * Method binds tags to news by given {@link Long} news id and {@link List} of {@link Tag} ids.
     * 
     * @param newsId id of news to be bound
     * @param tags list of tags to be bound
     * 
     * @throws DAOException
     */
	void bindTags(Long newsId, List<Tag> tags) throws DAOException;	
	
	/**
     * Method binds author to news by given {@link Long} news id and {@link Author} author id.
     * 
     * @param newsId id of news to be bound
     * @param authorId id of author to be bound
     * 
     * @throws DAOException
     */
	void bindAuthor(Long newsId, Long authorId) throws DAOException;
	
	/**
     * Method finds news according to {@link SearchCriteria} criteria and sorts by most commented news
     * and modification date.
     * 
     * @param searchCriteria criteria of searching for news
     * 
     * @return {@link List} of {@link News} objects
     * 
     * @throws DAOException
     */
	List<News> search(SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method unbinds tags by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @throws DAOException
     */
	void unbindTags(Long newsId) throws DAOException;
	
	/**
     * Method unbinds author by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @throws DAOException
     */
	void unbindAuthor(Long newsId) throws DAOException;
}
