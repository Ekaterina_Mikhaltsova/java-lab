package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.news.dao.ICommentDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Comment;
import com.epam.news.utils.DBUtil;
import com.epam.news.utils.exception.DBUtilException;

/**
 * Class implementation of {@link ICommentDAO} interface.
 * <p>
 * Implements all operations on COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentDAOImpl implements ICommentDAO {

	private static final String SQL_INSERT_COMMENT = "INSERT into comments (comment_id,comment_text,news_id) "
			+ "VALUES (comments_seq.nextval,?,?)";	
	
	private static final String SQL_DELETE_COMMENT = "DELETE from comments WHERE comment_id=?";
	
	private static final String SQL_FIND_COMMENTS_BY_NEWS_ID = "SELECT comment_id, comment_text, creation_date "
			+ "FROM comments WHERE news_id=?";
	
	private DataSource dataSource;

	public Long add(Comment comment) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] column = {"comment_id"};
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_INSERT_COMMENT, column);
			ps.setString(1, comment.getText());
			ps.setLong(2, comment.getNewsId());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				return -1L;
			}
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new comment", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	public void remove(Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_COMMENT);
			ps.setLong(1, commentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing a comment", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}
	
	public List<Comment> findByNewsId(Long newsId) throws DAOException {
		List<Comment> comments = new ArrayList<Comment>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_COMMENTS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getLong("comment_id"));
				comment.setText(rs.getString("comment_text"));
				comment.setCreationDate(rs.getDate("creation_date"));
				comment.setNewsId(newsId);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for comment", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return comments;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
