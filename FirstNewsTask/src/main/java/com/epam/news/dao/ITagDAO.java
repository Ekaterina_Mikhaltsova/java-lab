package com.epam.news.dao;

import java.util.List;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Tag;

/**
 * Interface that represents a contract for a DAO for the {@link Tag} model.
 * <p>
 * Contains methods for interaction with database TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface ITagDAO {

	/**
     * Method finds tags by given {@link Long} news id.
     * 
     * @return found {@link List} of {@link Tag} tags
     * 
     * @throws DAOException
     */
	List<Tag> findByNewsId(Long newsId) throws DAOException;
}
