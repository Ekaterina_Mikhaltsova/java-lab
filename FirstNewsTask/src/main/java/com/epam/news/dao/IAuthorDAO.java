package com.epam.news.dao;

import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Author;

/**
 * Interface that represents a contract for a DAO for the {@link Author} model.
 * <p>
 * Contains methods for interaction with database AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface IAuthorDAO {	
	
	/**
     * Method adds given author to the AUTHORS table.
     * 
     * @param author the author to be added
     * 
     * @return {@link Long} id of created author
     * 
     * @throws DAOException
     */
	Long add(Author author) throws DAOException;
	
	/**
     * Method makes given author expired.
     * 
     * @param authorId id of author to be made expired
     * 
     * @throws DAOException
     */
	void expired(Long authorId) throws DAOException;
	
	/**
     * Method finds author by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @return found {@link Author} author
     * 
     * @throws DAOException
     */
	Author findByNewsId(Long newsId) throws DAOException;
}
