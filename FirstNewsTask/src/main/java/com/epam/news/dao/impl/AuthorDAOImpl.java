package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import com.epam.news.dao.IAuthorDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Author;
import com.epam.news.utils.DBUtil;
import com.epam.news.utils.exception.DBUtilException;

/**
 * Class implementation of {@link IAuthorDAO} interface.
 * <p>
 * Implements all operations on AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorDAOImpl implements IAuthorDAO {

	private static final String SQL_INSERT_AUTHOR = "INSERT into authors (author_id,author_name,expired) "
			+ "VALUES (authors_seq.nextval,?,?)";
	
	private static final String SQL_FIND_AUTHOR_BY_NEWS_ID = "SELECT authors.author_id, authors.author_name, "
			+ "authors.expired FROM authors INNER JOIN news_authors "
			+ "ON authors.author_id = news_authors.author_id WHERE news_authors.news_id=?";
	
	private static final String SQL_SET_EXPIRED = "UPDATE authors SET expired=? WHERE author_id=?";
	
	private DataSource dataSource;
	
	public Long add(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String[] column = {"author_id"};
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_INSERT_AUTHOR, column);
			ps.setString(1, author.getName());
			ps.setDate(2, author.getExpired());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				return rs.getLong(1);
			} else {
				return -1L;
			}
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new author", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}		
	}

	public void expired(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_SET_EXPIRED);
			ps.setDate(1, new Date(System.currentTimeMillis()));
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while making author expired", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}	

	public Author findByNewsId(Long newsId) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		Author author = new Author();
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_AUTHOR_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author.setId(rs.getLong("author_id"));
				author.setName(rs.getString("author_name"));
				author.setExpired(rs.getDate("expired"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for author", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return author;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
