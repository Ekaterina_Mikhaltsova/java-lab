package com.epam.news.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.news.dao.ITagDAO;
import com.epam.news.dao.exception.DAOException;
import com.epam.news.entity.Tag;
import com.epam.news.utils.DBUtil;
import com.epam.news.utils.exception.DBUtilException;

/**
 * Class implementation of {@link ITagDAO} interface.
 * <p>
 * Implements all operations on TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagDAOImpl implements ITagDAO {

	private static final String SQL_FIND_TAGS_BY_NEWS_ID = "SELECT tags.tag_id, tags.tag_name FROM tags "
			+ "INNER JOIN news_tags ON tags.tag_id = news_tags.tag_id WHERE news_id=?";
	
	private DataSource dataSource;
	
	public List<Tag> findByNewsId(Long newsId) throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for tags", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return tags;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
