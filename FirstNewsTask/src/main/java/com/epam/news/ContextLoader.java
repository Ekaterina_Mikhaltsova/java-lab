package com.epam.news;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ContextLoader {

	public static ApplicationContext ac;
	public static DataSource dataSource;
	private static ContextLoader instance;
	
	public synchronized static ContextLoader getInstance() {
        if (instance == null) {
            instance = new ContextLoader();
        }
        return instance;
    }	
	
	public ContextLoader() {
		ac = new ClassPathXmlApplicationContext("spring.xml");
		dataSource = (DataSource) ac.getBean("dataSource");
    }	
	
	public ApplicationContext getApplicationContext() {
		return ac;
	}
}
