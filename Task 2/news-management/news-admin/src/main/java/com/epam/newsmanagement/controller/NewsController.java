package com.epam.newsmanagement.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.SearchCriteria;

@Controller
public class NewsController {

	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private TagService tagService;
	
	@Autowired
	private NewsService newsService;
	
	@Autowired
	private NewsManagementService newsManagementService;

	private int NEWS_ON_PAGE = 3;
	
	@Autowired
	public NewsController(MessageSource messageSource) {
		try {
			NEWS_ON_PAGE = Integer.parseInt(messageSource.getMessage(
					"news.on.page", null, Locale.US));
		} catch (NumberFormatException e) {
		}
	}
	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/filter-news")
	public ModelAndView newsFilter(HttpSession session,
			HttpServletRequest request,
			@RequestParam(value = "author", required = false) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds)
			throws ServiceException {
		return newsFilterModel(session, request, authorId, tagIds, 1);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/filter-news/{pageNumber}")
	public ModelAndView newsFilter(HttpSession session,
			HttpServletRequest request, @PathVariable Integer pageNumber)
			throws ServiceException {
		return newsFilterModel(session, request,
				(Long) session.getAttribute("authorId"),
				(List<Long>) session.getAttribute("tagIds"), pageNumber);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/reset-filter")
	public ModelAndView resetFilter(HttpSession session,
			HttpServletRequest request) throws ServiceException {
		session.setAttribute("authorId", null);
		session.setAttribute("tagIds", null);
		return newsFilterModel(session, request, null, null, 1);
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/single-news/{newsId}")
	public String singleNews(@PathVariable Long newsId, Model model,
			RedirectAttributes attr) throws ServiceException {
		NewsVO newsVO = newsManagementService.findByNewsId(newsId);
		if (!model.containsAttribute("comment")) {
			model.addAttribute("comment", new Comment());
		}
		model.addAttribute("news", newsVO);
		return "single-news";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-news", method = RequestMethod.POST)
	public String addNews(@Valid @ModelAttribute("news") News news,
			BindingResult result, Model model, RedirectAttributes attr,
			HttpSession session,
			@RequestParam(value = "author", required = true) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds)
			throws ServiceException {
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.news", result);
			attr.addFlashAttribute("news", news);
			return "redirect:/add-news-view";
		} else {
			List<Tag> tags = new ArrayList<Tag>();
			if (tagIds != null) {
				for (Long tagId : tagIds) {
					Tag tag = new Tag(tagId, null);
					tags.add(tag);
				}
			}
			NewsVO newsVO = new NewsVO(news, new Author(authorId, null, null),
					tags, null);
			Long newsId = newsManagementService.add(newsVO);
			return "redirect:/single-news/" + newsId;
		}
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/edit-news/{newsId}", method = RequestMethod.POST)
	public String editNews(@Valid @ModelAttribute("news") News news,
			BindingResult result, Model model, RedirectAttributes attr,
			HttpSession session, @PathVariable Long newsId,
			@RequestParam(value = "author", required = true) Long authorId,
			@RequestParam(value = "tags", required = false) List<Long> tagIds)
			throws ServiceException {
		if (result.hasErrors()) {
			news.setId(newsId);
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.news", result);
			attr.addFlashAttribute("news", news);
			return "redirect:/edit-news-view/" + newsId;
		} else {
			news.setId(newsId);
			List<Tag> tags = new ArrayList<Tag>();
			if (tagIds != null) {
				for (Long tagId : tagIds) {
					Tag tag = new Tag(tagId, null);
					tags.add(tag);
				}
			}
			NewsVO newsVO = new NewsVO(news, new Author(authorId, null, null),
					tags, null);
			newsManagementService.edit(newsVO);
			return "redirect:/single-news/" + newsId;
		}
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/delete-news", method = RequestMethod.POST)
	public String deleteNews(
			HttpSession session,
			@RequestParam(value = "newsToDelete", required = false) List<Long> newsToDelete)
			throws ServiceException {
		if (newsToDelete != null) {
			for (Long newsId : newsToDelete) {
				News news = new News();
				news.setId(newsId);
				newsManagementService.remove(news);
			}
		}
		return "redirect:/filter-news";
	}

	private ModelAndView newsFilterModel(HttpSession session,
			HttpServletRequest request, Long authorId, List<Long> tagIds,
			Integer pageNumber) throws ServiceException {
		ModelAndView model = new ModelAndView();
		int pageQuantity = 1;
		int newsQuantity = newsService.countNews(buildCriteria(authorId,
				tagIds, session));
		if (newsQuantity % NEWS_ON_PAGE == 0) {
			pageQuantity = newsQuantity / NEWS_ON_PAGE;
		} else {
			pageQuantity = newsQuantity / NEWS_ON_PAGE + 1;
		}
		List<News> news = newsService.search(
				buildCriteria(authorId, tagIds, session), pageNumber, NEWS_ON_PAGE);
		List<Author> authors = authorService.findAll();
		List<Tag> tags = tagService.findAll();
		List<NewsVO> newsVOList = newsManagementService.findAll(news);
		if (newsVOList.isEmpty()) {
			session.setAttribute("news", null);
		} else {
			session.setAttribute("news", news);
			model.addObject("news", newsVOList);
		}
		model.addObject("tags", tags);
		model.addObject("authors", authors);
		model.addObject("pageQuantity", pageQuantity);
		model.addObject("currentPage", pageNumber);
		model.setViewName("filter-news");
		return model;
	}

	private SearchCriteria buildCriteria(Long authorId, List<Long> tagIds,
			HttpSession session) {
		SearchCriteria searchCriteria = new SearchCriteria();
		if (authorId != null && authorId > 0L) {
			searchCriteria.setAuthorId(authorId);
			session.setAttribute("authorId", authorId);
		}
		if (tagIds != null) {
			searchCriteria.setTagIds(tagIds);
			session.setAttribute("tagIds", tagIds);
		}
		return searchCriteria;
	}
}
