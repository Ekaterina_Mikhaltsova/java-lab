package com.epam.newsmanagement.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Class that represents a controller for {@link Author} model.
 * 
 * @author Kate
 *
 */
@Controller
public class AuthorController {

	@Autowired
	private AuthorService authorService;

	/**
     * Method updates author.
     * 
     * @param authorId
     * @param author 
     * @param result
     * @param attr
     * @param model
     * 
     * @throws ServiceException
     */	
	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/update-author")
	public String updateAuthor(
			@RequestParam(value = "authorId", required = true) Long authorId,
			@Valid @ModelAttribute("author") Author author,
			BindingResult result, RedirectAttributes attr, Model model)
			throws ServiceException {
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.author",
					result);
			attr.addFlashAttribute("author", author);
		} else {
			author.setId(authorId);
			authorService.update(author);
		}
		return "redirect:/add-author-view";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/expire-author/{authorId}")
	public String expireAuthor(@PathVariable Long authorId, Model model)
			throws ServiceException {
		authorService.expired(authorId);
		return "redirect:/add-author-view";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-author")
	public String addAuthor(@Valid @ModelAttribute("author") Author author,
			BindingResult result, RedirectAttributes attr, Model model)
			throws ServiceException {
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.author",
					result);
			attr.addFlashAttribute("author", author);
		} else {
			authorService.add(author);
		}
		return "redirect:/add-author-view";
	}
}
