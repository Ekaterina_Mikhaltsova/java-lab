package com.epam.newsmanagement.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

@Controller
public class TagController {

	@Autowired
	private TagService tagService;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/update-tag")
	public ModelAndView updateTag(
			@RequestParam(value = "tagId", required = true) Long tagId,
			@RequestParam(value = "tagName", required = true) String tagName)
			throws ServiceException {
		Tag tag = new Tag(tagId, tagName);
		tagService.update(tag);
		return new ModelAndView("redirect:/add-tag-view");
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/delete-tag/{tagId}")
	public ModelAndView deleteTag(@PathVariable Long tagId)
			throws ServiceException {
		tagService.delete(tagId);
		return new ModelAndView("redirect:/add-tag-view");
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-tag")
	public String addTag(@Valid @ModelAttribute("tag") Tag tag,
			BindingResult result, RedirectAttributes attr, Model model)
			throws ServiceException {
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.tag", result);
			attr.addFlashAttribute("tag", tag);
		} else {
			tagService.add(tag);
		}
		return "redirect:/add-tag-view";
	}
}
