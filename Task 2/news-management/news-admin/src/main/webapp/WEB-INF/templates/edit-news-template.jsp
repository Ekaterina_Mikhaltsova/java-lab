<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<link rel="stylesheet"
	href="<c:url value="/resources/css/style.css"></c:url>" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/css/ui.dropdownchecklist.standalone.css"></c:url>" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/css/ui.dropdownchecklist.themeroller.css"></c:url>" type="text/css" />
	<script type="text/javascript" src="<c:url value='/resources/js/script.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist.js'/>"></script>
	<title><tiles:getAsString name="title" /></title>
</head>

<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="logout-menu" />
	<tiles:insertAttribute name="locale" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>

</html>