<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="admin-page">
	<div class="filter" align="center">
		<table border="0">
			<tr>
				<td>
					<form:form name="filter" method="POST" action="/news-admin/filter-news">
						<select name="author">
							<c:if test="${authorId != null}">
								<option value="-1"><spring:message code="select.author" /></option>
							</c:if>
							<c:if test="${authorId == null}">
								<option selected="selected" value="-1"><spring:message code="select.author" /></option>
							</c:if>
							<c:forEach items="${authors}" var="author">
								<c:if test="${authorId != author.id}">
									<option value="${author.id}">
										<c:out value="${author.name}" />
									</option>
								</c:if>
								<c:if test="${authorId == author.id}">
									<option selected="selected" value="${author.id}">
										<c:out value="${author.name}" />
									</option>
								</c:if>
							</c:forEach>
						</select> 
						<select name="tags" multiple id="multi-dropbox">
							<c:forEach items="${tags}" var="tag">
								<c:set var="contains" value="false" />
								<c:forEach var="item" items="${tagIds}">
									<c:if test="${item == tag.id}">
										<c:set var="contains" value="true" />
									</c:if>
								</c:forEach>
								<c:if test="${contains == true}">
									<option selected value="${tag.id}">
										<c:out value="${tag.name}" />
									</option>
								</c:if>
								<c:if test="${contains != true}">
									<option value="${tag.id}">
										<c:out value="${tag.name}" />
									</option>
								</c:if>
							</c:forEach>
						</select> 
						<input type="submit" value="<spring:message code="filter.button" />" />
					</form:form>
				</td>
				<td>
					<form:form name="reset" method="POST" action="/news-admin/reset-filter">
						<input type="submit" value="<spring:message code="reset.button" />" />
					</form:form>
				</td>
			</tr>
		</table>
	</div>	
	<br>	
	<script type="text/javascript">
		$("#multi-dropbox").dropdownchecklist({
			emptyText : "<spring:message code="select.tags" />",
			width : 120
		});
	</script>
	<div align="center"><c:if test="${news == null}"><fmt:message key="no.news.message"/></c:if></div>
    <c:if test="${news != null}">
		<form:form name="deleteNews" action="/news-admin/delete-news" method="post">
			<c:forEach var="news" items="${news}">
				<div class="news-title">
					<a href="/news-admin/single-news/${news.news.id}"><c:out value="${news.news.title}" /></a>
				</div>
				<div class="simple">(<spring:message code="by.message" /><c:out value=" ${news.author.name}" />)</div>
				<div class="date">
					<fmt:formatDate pattern="dd/MM/yyyy" value="${news.news.modificationDate}" />
				</div><br><br>
				<c:out value="${news.news.shortText}" /><br>
				<div class="tag-comment" align="right">
					<div class="tags">
						<c:forEach var="tag" items="${news.tags}" varStatus="status">
							<c:out value="${tag.name}" />
							<c:if test="${!status.last}">,</c:if>
						</c:forEach>
					</div>
					<div class="comments"><spring:message code="comments.message" />(${fn:length(news.comments)})</div>
					<a href="/news-admin/edit-news-view/${news.news.id}"><spring:message code="edit.news.button" /></a>
					<input type="checkbox" name="newsToDelete" value="${news.news.id}" />
				</div><br><br>
			</c:forEach>
			<c:if test="${news != null}"><input class="right-button" type="submit" value="<fmt:message key="delete.news.button"/>"></c:if>
		</form:form>
	</c:if><br>
	<c:set var="currentPage" value="${currentPage}" />
	<div align="center">
		<div id="pag"></div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#pag").pagination({
				pages : "${pageQuantity}",
				hrefTextPrefix : "/news-admin/filter-news/",
				cssStyle : "light-theme",
				currentPage: "${currentPage}"
			});
		});
	</script>
</div>