<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="menu">
	<ul>
		<li><a href="/news-admin/filter-news"><spring:message code="news.list.message" /></a><br></li>
		<li><a href="/news-admin/add-news-view"><spring:message code="add.news.message" /></a><br></li>
		<li><a href="/news-admin/add-author-view"><spring:message code="update.authors.message" /></a><br></li>
		<li><a href="/news-admin/add-tag-view"><spring:message code="update.tags.message" /></a><br></li>
	</ul>
</div>