<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="admin-page">
	<c:forEach var="author" items="${authors}">
		<form:form id="updateForm${author.id}" action="/news-admin/update-author" method="post" modelAttribute="author">
			<strong><spring:message code="author.message" />:</strong> 
			<form:input path="name" type="text" name="authorName" id="text${author.id}" 
			value="${author.name}" 
			size="30" disabled="true" /> 
			<a id="edit${author.id}" href="javascript:{}" onclick="switchEnableInput(${author.id})"><spring:message code="edit.button" /></a> 
			<a id="update${author.id}" href="javascript:{}" onclick="updateFormLink(${author.id})" class="hidden"><spring:message code="update.button" /></a> 
			<a id="delete${author.id}" href="/news-admin/expire-author/${author.id}" class="hidden"><spring:message code="expire.button" /></a> 
			<a id="cancel${author.id}" href="javascript:{}" onclick="switchDisableInput(${author.id})" class="hidden"><spring:message code="cancel.button" /></a><br> 
			<input type="hidden" name="authorId" value="${author.id}" />
		</form:form>
	</c:forEach>
	<form:form action="/news-admin/add-author" name="addAuthor" method="post" modelAttribute="author">
		<strong><spring:message code="add.author.message" />:</strong> 
		<form:textarea path="name" cols="65" rows="1"></form:textarea><br>
		<form:errors path="name" cssClass="error" /><br>
		<input type="submit" value="<spring:message code="save.button" />">
	</form:form>
</div>