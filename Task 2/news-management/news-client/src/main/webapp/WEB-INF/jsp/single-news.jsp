<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<c:set var="lang" value="${not empty param.lang ? param.lang : not empty lang ? lang : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="content" />

<html>

<head>
	<link rel="stylesheet" href="<c:url value="/resources/css/style.css"></c:url>" type="text/css" />
	<title>Single news</title>
</head>

<body>
	<jsp:include page="header.jsp" />
	<c:if test="${authorId == null}"><a href="controller?command=filter"><fmt:message key="back.button" /></a></c:if>
	<c:if test="${authorId != null}"><a href="controller?command=filter&author=${authorId}"><fmt:message key="back.button" /></a></c:if>
	<br><br>
	<div class="content">
		<c:set var="news" value="${news}"/>
		<div class="news-title">${news.news.title}</div>
		<div class="simple">(<fmt:message key="by.message" /> ${news.author.name})</div>
		<div class="date">
			<fmt:formatDate pattern="dd/MM/yyyy" value="${news.news.modificationDate}" />
		</div><br><br>
		<div class="simple">${news.news.fullText}</div><br><br>
		<c:forEach var="comment" items="${news.comments}">
			<div class="comment-date">
				<fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
			</div><br>
			<div class="comment-text"><c:out value="${comment.text}" /></div><br><br>
		</c:forEach>
		<form name="addComment" method="POST" action="controller" >
			<textarea name="commentText" cols="83" rows="6"></textarea><br>
			<input type="hidden" name="command" value="addComment" />
			<input type="hidden" name="newsId" value="${news.news.id}" /><br>
			<div align="right" class="post-button"><input type="submit" value="<fmt:message key="post.comment.button" />" /></div>
		</form><br>
	</div>
	<jsp:include page="footer.jsp" />
</body>

</html>