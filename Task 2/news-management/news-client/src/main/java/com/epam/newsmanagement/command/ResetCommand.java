package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.service.exception.ServiceException;

public class ResetCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		session.setAttribute("authorId", null);
		session.setAttribute("tagIds", null);
		return new FilterNewsCommand().execute(request, session);
	}
}
