package com.epam.newsmanagement.command;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;

import com.epam.newsmanagement.controller.ContextLoader;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class AddCommentCommand implements ICommand {

	private CommentService commentService;
	
	private NewsManagementService newsManagementService;
	
	private MessageSource messageSource;

	public AddCommentCommand() {
		commentService = ContextLoader.getInstance().getCommonService().getCommentService();
		newsManagementService = ContextLoader.getInstance().getCommonService().getNewsManagementService();
		messageSource = ContextLoader.getInstance().getMessageSource();
	}

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		String page = messageSource.getMessage("single.news.page", null, Locale.US);
		Long newsId = Long.parseLong(request.getParameter("newsId"));
		String text = request.getParameter("commentText");
		NewsVO newsVO;
		if (text.length() <= 30) {
			Comment comment = new Comment();
			comment.setText(text);
			comment.setNewsId(newsId);
			commentService.add(comment);

		}
		newsVO = newsManagementService.findByNewsId(newsId);
		request.setAttribute("news", newsVO);
		return page;
	}
}
