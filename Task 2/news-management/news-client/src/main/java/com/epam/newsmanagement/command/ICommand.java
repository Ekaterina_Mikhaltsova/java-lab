package com.epam.newsmanagement.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.service.exception.ServiceException;

public interface ICommand {

	String execute(HttpServletRequest request, HttpSession session) throws ServiceException;
}
