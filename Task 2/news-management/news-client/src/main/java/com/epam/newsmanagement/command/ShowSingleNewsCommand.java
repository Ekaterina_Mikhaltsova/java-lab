package com.epam.newsmanagement.command;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;

import com.epam.newsmanagement.controller.ContextLoader;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.exception.ServiceException;

public class ShowSingleNewsCommand implements ICommand {

	private NewsManagementService newsManagementService;
	
	private MessageSource messageSource;
	
	public ShowSingleNewsCommand() {
		newsManagementService = ContextLoader.getInstance().getCommonService().getNewsManagementService();
		messageSource = ContextLoader.getInstance().getMessageSource();
	}
	
	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		String page = messageSource.getMessage("single.news.page", null, Locale.US);
		Long id = Long.parseLong(request.getParameter("id"));
		NewsVO newsVO = newsManagementService.findByNewsId(id);
		request.setAttribute("news", newsVO);
		return page;
	}
}
