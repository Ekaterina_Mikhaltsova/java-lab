package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.SearchCriteria;

/**
 * Interface that represents a service for the {@link News} model.
 * <p>
 * Contains methods for interaction with {@link NewsDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface NewsService {

	/**
     * Method adds given news.
     * 
     * @param news the news to be added
     * 
     * @return {@link Long} id of created news
     * 
     * @throws ServiceException
     */
	Long add(News news) throws ServiceException;
	
	/**
     * Method binds tags to news by given {@link Long} news id and {@link List} of {@link Tag} tags.
     * 
     * @param newsId id of news to be bound
     * @param tags list of tags to be bound
     * 
     * @throws ServiceException
     */
	void bindTags(Long newsId, List<Tag> tags) throws ServiceException;
	
	/**
     * Method unbinds tags from news by given {@link Long} news id.
     * 
     * @param newsId id of news to be unbound
     * 
     * @throws ServiceException
     */
	void unbindTags(Long newsId) throws ServiceException;
	
	/**
     * Method binds author to news by given {@link Long} news id and {@link Author} author id.
     * 
     * @param newsId id of news to be bound
     * @param authorId id of author to be bound
     * 
     * @throws ServiceException
     */
	void bindAuthor(Long newsId, Long authorId) throws ServiceException;
	
	/**
     * Method unbinds author from news by given {@link Long} news id.
     * 
     * @param newsId id of news to be unbound
     * 
     * @throws ServiceException
     */
	void unbindAuthor(Long newsId) throws ServiceException;
	
	/**
     * Method finds news by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link News} news
     * 
     * @throws ServiceException
     */
	News findById(Long newsId) throws ServiceException;
	
	/**
     * Method updates news by given {@link News} news.
     * 
     * @param news the news with updated information and same id
     * 
     * @throws ServiceException
     */
	void update(News news) throws ServiceException;
	
	/**
     * Method removes news by given {@link Long} news id.
     * 
     * @param newsId id of news to be removed
     * 
     * @throws ServiceException
     */
	void remove(Long newsId) throws ServiceException;
	
	/**
     * Method counts all news.
     * 
     * @param searchCriteria
     * 
     * @return news quantity
     * 
     * @throws ServiceException
     */
	int countNews(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
     * Method finds news according to {@link SearchCriteria} criteria and sorts by most commented news
     * and modification date.
     * 
     * @param searchCriteria criteria of searching for news
     * @param pageId id of page
     * @param newsOnPage quantity of news on page
     * 
     * @return {@link List} of {@link News} objects
     * 
     * @throws ServiceException
     */
	List<News> search(SearchCriteria searchCriteria, int pageId, int newsOnPage) throws ServiceException;
}
