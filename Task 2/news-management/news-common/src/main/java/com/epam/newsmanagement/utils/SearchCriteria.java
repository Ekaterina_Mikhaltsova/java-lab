package com.epam.newsmanagement.utils;

import java.util.List;

/**
 * Class represents SearchCriteria model.
 *
 * @author Kate
 */
public class SearchCriteria {

	/**
	 * Author id, whose news are to be found.
	 */
	private Long authorId;
	
	/**
	 * {@link List} of tag ids, that news to be found has.
	 */
	private List<Long> tagIds;

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagIds() {
		return tagIds;
	}

	public void setTagIds(List<Long> tagIds) {
		this.tagIds = tagIds;
	}
}
