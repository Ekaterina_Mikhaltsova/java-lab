package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.DBUtil;
import com.epam.newsmanagement.utils.SearchCriteria;
import com.epam.newsmanagement.utils.exception.DBUtilException;

/**
 * Class implementation of {@link NewsDAO} interface.
 * <p>
 * Implements all operations on NEWS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class NewsDAOImpl implements NewsDAO {

	private static final String SQL_INSERT_NEWS = "INSERT into news (news_id,title,short_text,full_text) "
			+ "VALUES (news_seq.nextval,?,?,?)";
	
	private static final String SQL_FIND_NEWS_BY_ID = "SELECT news_id, title, short_text, full_text, "
			+ "creation_date, modification_date FROM news WHERE news_id=?";
	
	private static final String SQL_UPDATE_NEWS = "UPDATE news SET title=?, short_text=?, full_text=?, "
			+ "modification_date=? WHERE news_id=?";
	
	private static final String SQL_DELETE_NEWS = "DELETE from news WHERE news_id=?";
	
	private static final String SQL_ADD_TAGS = "INSERT into news_tags (nt_news_id,nt_tag_id) "
			+ "VALUES (?,?)";
	
	private static final String SQL_ADD_AUTHOR = "INSERT into news_authors (na_news_id,na_author_id) "
			+ "VALUES (?,?)";
	
	private static final String SQL_DELETE_TAGS = "DELETE from news_tags WHERE nt_news_id=?";
	
	private static final String SQL_DELETE_AUTHOR = "DELETE from news_authors WHERE na_news_id=?";
	
	@Autowired
	private DataSource dataSource;

	@Override
	public Long add(News news) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long newsId;
		String[] column = {"news_id"};
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_INSERT_NEWS, column);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				newsId = rs.getLong(1);
			} else {
				newsId = -1L;
			}
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new news", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return newsId;
	}

	@Override
	public News findById(Long newsId) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		News news = new News();
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_NEWS_BY_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				news.setId(rs.getLong("news_id"));
				news.setTitle(rs.getString("title"));
				news.setShortText(rs.getString("short_text"));
				news.setFullText(rs.getString("full_text"));
				news.setCreationDate(rs.getDate("creation_date"));
				news.setModificationDate(rs.getDate("modification_date"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for news", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return news;
	}

	@Override
	public void update(News news) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_UPDATE_NEWS);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setDate(4, new Date(System.currentTimeMillis()));
			ps.setLong(5, news.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating news", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void remove(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_NEWS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing news", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}
	
	@Override
	public int countNews(SearchCriteria searchCriteria) throws DAOException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(buildQuery(searchCriteria));			
			if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() != null) {
				ps.setLong(1, searchCriteria.getAuthorId());
				int i = 2;
				for (Long tagId : searchCriteria.getTagIds()) {
					ps.setLong(i,  tagId);
					i++;
				}
			} else {
				if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() == null) {
					ps.setLong(1, searchCriteria.getAuthorId());
				} else {
					if (searchCriteria.getAuthorId() == null && searchCriteria.getTagIds() != null) {
						int i = 1;
						for (Long tagId : searchCriteria.getTagIds()) {
							ps.setLong(i,  tagId);
							i++;
						}
					}
				}
			}
			rs = ps.executeQuery();
			if (rs.next()) {
				return rs.getInt("total");
			} else return 0;
		} catch (SQLException e) {
			throw new DAOException("Error while counting news", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}
	
	@Override
	public void bindTags(Long newsId, List<Tag> tags) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_TAGS);
			for (Tag tag : tags) {
				ps.setLong(1, newsId);
				ps.setLong(2, tag.getId());		
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			throw new DAOException("Error while adding tags to news", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void bindAuthor(Long newsId, Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_AUTHOR);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while adding author to news", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void unbindTags(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_TAGS);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing tags", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void unbindAuthor(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_AUTHOR);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing author", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}
	
	@Override
	public List<News> search(SearchCriteria searchCriteria, int pageId, int start, int end) throws DAOException {
		ResultSet rs = null;
		PreparedStatement ps = null;
		Connection con = null;
		List<News> news = new ArrayList<News>();
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(buildQuery(searchCriteria, pageId, start, end));
			ps = prepareStatement(ps, searchCriteria);
			rs = ps.executeQuery();
			while (rs.next()) {
				News newsObj = new News();
				newsObj.setId(rs.getLong("news_id"));
				newsObj.setTitle(rs.getString("title"));
				newsObj.setShortText(rs.getString("short_text"));
				newsObj.setFullText(rs.getString("full_text"));
				newsObj.setCreationDate(rs.getDate("creation_date"));
				newsObj.setModificationDate(rs.getDate("modification_date"));
				news.add(newsObj);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while reading news", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return news;
	}
	
	private PreparedStatement prepareStatement(PreparedStatement ps, SearchCriteria searchCriteria) throws SQLException {
		if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() != null) {
			ps.setLong(1, searchCriteria.getAuthorId());
			int i = 2;
			for (Long tagId : searchCriteria.getTagIds()) {
				ps.setLong(i,  tagId);
				i++;
			}
		} else {
			if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() == null) {
				ps.setLong(1, searchCriteria.getAuthorId());
			} else {
				if (searchCriteria.getAuthorId() == null && searchCriteria.getTagIds() != null) {
					int i = 1;
					for (Long tagId : searchCriteria.getTagIds()) {
						ps.setLong(i,  tagId);
						i++;
					}
				}
			}
		}
		return ps;
	}
	
	private String buildQuery(SearchCriteria searchCriteria, int pageId, int start, int end) {
		StringBuilder query = new StringBuilder();
		int maxRow = pageId * end;
		int minRow = maxRow - start;		
		query.append("SELECT * FROM (SELECT news.news_id, news.title, news.short_text, news.full_text, "
				+ "news.creation_date, news.modification_date, ROWNUM rnum FROM (SELECT news.news_id, news.title, news.short_text, news.full_text, "
				+ "news.creation_date, news.modification_date FROM news");
		if (searchCriteria.getAuthorId() != null) {
			query.append(" INNER JOIN news_authors ON news.news_id = news_authors.na_news_id");
		}
		if (searchCriteria.getTagIds() != null) {
			query.append(" INNER JOIN news_tags ON news.news_id = news_tags.nt_news_id");
		}
		query.append(" LEFT JOIN comments ON news.news_id = comments.c_news_id");
		if (searchCriteria.getAuthorId() != null || searchCriteria.getTagIds() != null) {
			query.append(" WHERE");
		}
		if (searchCriteria.getAuthorId() != null) {
			query.append(" na_author_id = ?");
		}
		if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() != null) {
			query.append(" AND");
		}
		if (searchCriteria.getTagIds() != null) {
			query.append(" nt_tag_id IN(");
			for (int i = 0; i < searchCriteria.getTagIds().size(); i++) {
				query.append("?,");
			}
			query.deleteCharAt(query.length() - 1);
			query.append(")");
		}
		query.append(" GROUP BY news.news_id, news.title, news.short_text, news.full_text, news.creation_date, "
				+ "news.modification_date ORDER BY COUNT(comments.c_news_id) DESC, news.modification_date DESC) "
				+ "news WHERE ROWNUM <= " + maxRow + " ) WHERE rnum >= " + minRow);
		return query.toString();		
	}
	
	private String buildQuery(SearchCriteria searchCriteria) {
		StringBuilder query = new StringBuilder();
		query.append("SELECT COUNT(*) AS total FROM news");
		if (searchCriteria.getAuthorId() != null) {
			query.append(" INNER JOIN news_authors ON news.news_id = news_authors.na_news_id");
		}
		if (searchCriteria.getTagIds() != null) {
			query.append(" INNER JOIN news_tags ON news.news_id = news_tags.nt_news_id");
		}
		if (searchCriteria.getAuthorId() != null || searchCriteria.getTagIds() != null) {
			query.append(" WHERE");
		}
		if (searchCriteria.getAuthorId() != null) {
			query.append(" na_author_id = ?");
		}
		if (searchCriteria.getAuthorId() != null && searchCriteria.getTagIds() != null) {
			query.append(" AND");
		}
		if (searchCriteria.getTagIds() != null) {
			query.append(" nt_tag_id IN(");
			for (int i = 0; i < searchCriteria.getTagIds().size(); i++) {
				query.append("?,");
			}
			query.deleteCharAt(query.length() - 1);
			query.append(")");
		}
		return query.toString();		
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
