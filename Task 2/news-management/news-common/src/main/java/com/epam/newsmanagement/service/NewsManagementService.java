package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Interface that represents a service for the {@link NewsVO} model.
 * <p>
 * Contains transactional methods for interacting with linked database tables.
 * </p>
 * 
 * @author Kate
 *
 */
public interface NewsManagementService {

	/**
     * Method adds given newsVO.
     * 
     * @param newsVO the newsVO to be added
     * 
     * @return {@link Long} id of created newsVO
     * 
     * @throws ServiceException
     */
	Long add(NewsVO newsVO) throws ServiceException;
	
	/**
     * Method updates given newsVO.
     * 
     * @param newsVO the newsVO to be updated
     * 
     * @throws ServiceException
     */
	void edit(NewsVO newsVO) throws ServiceException;
	
	/**
     * Method removes given news.
     * 
     * @param news the news to be removed
     * 
     * @throws ServiceException
     */
	void remove(News news) throws ServiceException;
	
	/**
     * Method finds newsVO by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link NewsVO} object
     * 
     * @throws ServiceException
     */
	NewsVO findByNewsId(Long newsId) throws ServiceException;
	
	
	/**
     * Method finds all newsVO objects by given {@link List} of news.
     * 
     * @param news list of found news
     * 
     * @return found {@link List} of {@link NewsVO} objects
     * 
     * @throws ServiceException
     */
	List<NewsVO> findAll(List<News> news) throws ServiceException;
}
