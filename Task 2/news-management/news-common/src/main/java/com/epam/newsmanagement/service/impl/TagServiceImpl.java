package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Class implementation of {@link TagService} interface.
 * <p>
 * Implements all operations defined in {@link TagService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagServiceImpl implements TagService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private TagDAO tagDAO;
	
	@Override
	public void add(Tag tag) throws ServiceException {
		try {
			tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while addding tag", e);
		}
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws ServiceException {
		try {
			return tagDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding tags by news ID", e);
		}
	}

	@Override
	public List<Tag> findAll() throws ServiceException {
		try {
			return tagDAO.findAll();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding all tags", e);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while updating tag", e);
		}
	}
	
	@Transactional(rollbackFor = {ServiceException.class, RuntimeException.class})
	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDAO.deleteFromNews(tagId);
			tagDAO.delete(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while deleting tag", e);
		}
	}

	public TagDAO getTagDAO() {
		return tagDAO;
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
}
