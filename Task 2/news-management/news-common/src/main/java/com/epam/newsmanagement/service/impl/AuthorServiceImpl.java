package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Class implementation of {@link AuthorService} interface.
 * <p>
 * Implements all operations defined in {@link AuthorService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorServiceImpl implements AuthorService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	@Autowired
	private AuthorDAO authorDAO;
	
	@Override
	public void add(Author author) throws ServiceException {
		try {
			authorDAO.add(author);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding author", e);
		}
	}

	@Override
	public void update(Author author) throws ServiceException {
		try {
			authorDAO.update(author);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while updating author", e);
		}
	}

	@Override
	public Author findByNewsId(Long newsId) throws ServiceException {
		try {
			return authorDAO.findByNewsId(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding author by news ID", e);
		}
	}

	@Override
	public void expired(Long authorId) throws ServiceException {
		try {
			authorDAO.expired(authorId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while making author expired", e);
		}
	}

	@Override
	public List<Author> findAll() throws ServiceException {
		try {
			return authorDAO.findAll();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding authors", e);
		}
	}	

	public AuthorDAO getAuthorDAO() {
		return authorDAO;
	}

	public void setAuthorDAO(AuthorDAO authorDAO) {
		this.authorDAO = authorDAO;
	}
}
