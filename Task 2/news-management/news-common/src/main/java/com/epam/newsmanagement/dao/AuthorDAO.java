package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;

/**
 * Interface that represents a contract for a DAO for the {@link Author} model.
 * <p>
 * Contains methods for interaction with database AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface AuthorDAO {	
	
	/**
     * Method adds given author to the AUTHORS table.
     * 
     * @param author the author to be added
     * 
     * @throws DAOException
     */
	void add(Author author) throws DAOException;
	
	/**
     * Method makes given author expired.
     * 
     * @param authorId id of author to be made expired
     * 
     * @throws DAOException
     */
	void expired(Long authorId) throws DAOException;
	
	/**
     * Method finds author by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @return found {@link Author} author
     * 
     * @throws DAOException
     */
	Author findByNewsId(Long newsId) throws DAOException;
	
	/**
     * Method finds all authors.
     * 
     * @return found {@link List} of {@link Author} authors
     * 
     * @throws DAOException
     */
	List<Author> findAll() throws DAOException;
	
	/**
     * Method updates given author.
     * 
     * @param author author to be updated
     * 
     * @throws DAOException
     */
	void update(Author author) throws DAOException;
}
