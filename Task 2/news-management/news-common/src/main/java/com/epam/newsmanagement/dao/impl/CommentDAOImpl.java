package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.utils.DBUtil;
import com.epam.newsmanagement.utils.exception.DBUtilException;

/**
 * Class implementation of {@link CommentDAO} interface.
 * <p>
 * Implements all operations on COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentDAOImpl implements CommentDAO {

	private static final String SQL_INSERT_COMMENT = "INSERT into comments (comment_id,comment_text,"
			+ "creation_date,c_news_id) VALUES (comments_seq.nextval,?,?,?)";	
	
	private static final String SQL_DELETE_COMMENT = "DELETE from comments WHERE comment_id=?";
	
	private static final String SQL_FIND_COMMENTS_BY_NEWS_ID = "SELECT comment_id, comment_text, creation_date "
			+ "FROM comments WHERE c_news_id=?";
	
	private static final String SQL_DELETE_COMMENTS_BY_NEWS_ID = "DELETE from comments WHERE c_news_id=?";
	
	private DataSource dataSource;

	@Override
	public void add(Comment comment) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_INSERT_COMMENT);
			ps.setString(1, comment.getText());
			ps.setDate(2, new Date(System.currentTimeMillis()));
			ps.setLong(3, comment.getNewsId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new comment", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void remove(Long commentId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_DELETE_COMMENT);
			ps.setLong(1, commentId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing a comment", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}
	
	@Override
	public void removeCommentFromNews(Long newsId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_COMMENTS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while removing comments", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public List<Comment> findByNewsId(Long newsId) throws DAOException {
		List<Comment> comments = new ArrayList<Comment>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_COMMENTS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Comment comment = new Comment();
				comment.setId(rs.getLong("comment_id"));
				comment.setText(rs.getString("comment_text"));
				comment.setCreationDate(rs.getDate("creation_date"));
				comment.setNewsId(newsId);
				comments.add(comment);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for comment", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return comments;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

}
