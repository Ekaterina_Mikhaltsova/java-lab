package com.epam.newsmanagement.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.utils.exception.DBUtilException;

/**
 * Util class for interaction with database resources.
 * <p>
 * Contains static methods closing different database resources.
 * </p>
 * 
 * @author Kate
 *
 */
public class DBUtil {

	/**
     * Static method for closing {@link ResultSet}, {@link PreparedStatement} and {@link Connection}.
     * 
     * @param rs {@link ResultSet} object
     * @param ps {@link PreparedStatement} object
     * @param con {@link Connection} object
     * 
     * @throws DBUtilException
     */
	public static void close(ResultSet rs, PreparedStatement ps, Connection con) throws DBUtilException {
		try {
				rs.close();
		} catch (SQLException e) {
			throw new DBUtilException("Failed to close ResultSet", e);
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				throw new DBUtilException("Failed to close PreparedStatement", e);
			} finally {
				try {
					con.close();
				} catch (SQLException e) {
					throw new DBUtilException("Failed to close Connection", e);
				}
			}
		}
	}
	
	/**
     * Static method for closing {@link ResultSet}, {@link Statement} and {@link Connection}.
     * 
     * @param rs {@link ResultSet} object
     * @param ps {@link Statement} object
     * @param con {@link Connection} object
     * 
     * @throws DBUtilException
     */
	public static void close(ResultSet rs, Statement st, Connection con) throws DBUtilException {
		try {
			rs.close();
		} catch (SQLException e) {
			throw new DBUtilException("Failed to close ResultSet", e);
		} finally {
			try {
				st.close();
			} catch (SQLException e) {
				throw new DBUtilException("Failed to close Statement", e);
			} finally {
				try {
					con.close();
				} catch (SQLException e) {
					throw new DBUtilException("Failed to close Connection", e);
				}
			}
		}
	}
	
	
	/**
     * Static method for closing {@link PreparedStatement} and {@link Connection}.
     * 
     * @param ps {@link PreparedStatement} object
     * @param con {@link Connection} object
     * 
     * @throws DBUtilException
     */
	public static void close(PreparedStatement ps, Connection con) throws DBUtilException {
		try {
			ps.close();
		} catch (SQLException e) {
			throw new DBUtilException("Failed to close Statement", e);
		} finally {
			try {
				con.close();
			} catch (SQLException e) {
				throw new DBUtilException("Failed to close Connection", e);
			}
		}
	}
	
	/**
     * Static method for closing {@link ResultSet}, {@link PreparedStatement}, {@link Connection}
     * and {@link DataSource}.
     * 
     * @param rs {@link ResultSet} object
     * @param ps {@link PreparedStatement} object
     * @param con {@link Connection} object
     * @param dataSource {@link DataSource} object
     * 
     * @throws DBUtilException
     */
	public static void close(ResultSet rs, PreparedStatement ps, Connection con, 
			DataSource dataSource) throws DBUtilException {
		try {
			rs.close();
		} catch (SQLException e) {
			throw new DBUtilException("Failed to close ResultSet", e);
		} finally {
			try {
				ps.close();
			} catch (SQLException e) {
				throw new DBUtilException("Failed to close PreparedStatement", e);
			} finally {
				DataSourceUtils.releaseConnection(con, dataSource);
			}
		}
	}
	
	/**
     * Static method for closing {@link PreparedStatement}, {@link Connection}
     * and {@link DataSource}.
     * 
     * @param ps {@link PreparedStatement} object
     * @param con {@link Connection} object
     * @param dataSource {@link DataSource} object
     * 
     * @throws DBUtilException
     */
	public static void close(PreparedStatement ps, Connection con,
			DataSource dataSource) throws DBUtilException {
		try {
			ps.close();
		} catch (SQLException e) {
			throw new DBUtilException("Failed to close PreparedStatement", e);
		} finally {
			DataSourceUtils.releaseConnection(con, dataSource);
		}
	}
}
