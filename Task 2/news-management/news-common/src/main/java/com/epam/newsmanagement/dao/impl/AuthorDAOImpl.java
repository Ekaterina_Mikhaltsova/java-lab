package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.utils.DBUtil;
import com.epam.newsmanagement.utils.exception.DBUtilException;

/**
 * Class implementation of {@link AuthorDAO} interface.
 * <p>
 * Implements all operations on AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorDAOImpl implements AuthorDAO {

	private static final String SQL_INSERT_AUTHOR = "INSERT into authors (author_id,author_name,expired) "
			+ "VALUES (authors_seq.nextval,?,?)";
	
	private static final String SQL_FIND_AUTHOR_BY_NEWS_ID = "SELECT authors.author_id, authors.author_name, "
			+ "authors.expired FROM authors INNER JOIN news_authors "
			+ "ON authors.author_id = news_authors.na_author_id WHERE news_authors.na_news_id=?";
	
	private static final String SQL_FIND_ALL_AUTHORS = "SELECT author_id, author_name, expired "
			+ "FROM authors";
	
	private static final String SQL_UPDATE_AUTHOR = "UPDATE authors SET author_name=? WHERE author_id=?";
	
	private static final String SQL_SET_EXPIRED = "UPDATE authors SET expired=? WHERE author_id=?";
	
	private DataSource dataSource;
	
	@Override
	public void add(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_INSERT_AUTHOR);
			ps.setString(1, author.getName());
			ps.setDate(2, author.getExpired());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new author", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}		
	}

	@Override
	public void update(Author author) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_UPDATE_AUTHOR);
			ps.setString(1, author.getName());
			ps.setLong(2, author.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating author", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void expired(Long authorId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_SET_EXPIRED);
			ps.setDate(1, new Date(System.currentTimeMillis()));
			ps.setLong(2, authorId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while making author expired", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}	

	@Override
	public Author findByNewsId(Long newsId) throws DAOException {
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		Author author = new Author();
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_AUTHOR_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs.next()) {
				author.setId(rs.getLong("author_id"));
				author.setName(rs.getString("author_name"));
				author.setExpired(rs.getDate("expired"));
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for author", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return author;
	}

	@Override
	public List<Author> findAll() throws DAOException {
		Statement st = null;
		ResultSet rs = null;
		Connection con = null;
		List<Author> authors = new ArrayList<Author>();
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQL_FIND_ALL_AUTHORS);
			while (rs.next()) {
				Author author = new Author();
				author.setId(rs.getLong("author_id"));
				author.setName(rs.getString("author_name"));
				author.setExpired(rs.getDate("expired"));
				authors.add(author);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for authors", e);
		} finally {
			try {
				DBUtil.close(rs, st, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return authors;
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
