package com.epam.newsmanagement.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.NewsVO;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsManagementService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.service.exception.ServiceException;

/**
 * Class implementation of {@link NewsManagementService} interface.
 * <p>
 * Implements all operations defined in {@link NewsManagementService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
@Transactional(rollbackFor = {ServiceException.class, RuntimeException.class})
public class NewsManagementServiceImpl implements NewsManagementService {
		
	@Autowired
	private NewsService newsService;
		
	@Autowired
	private AuthorService authorService;	
	
	@Autowired
	private TagService tagService;	
	
	@Autowired
	private CommentService commentService;	
	
	@Override
	public Long add(NewsVO newsVO) throws ServiceException {
		News news = newsVO.getNews();
		Long newsId = newsService.add(news);
		if (!newsVO.getTags().isEmpty()) {
			newsService.bindTags(newsId, newsVO.getTags());
		}
		newsService.bindAuthor(newsId, newsVO.getAuthor().getId());
		return newsId;
	}	
	
	@Override
	public void edit(NewsVO newsVO) throws ServiceException {
		News news = newsVO.getNews();
		Long newsId = news.getId();
		newsService.unbindAuthor(newsId);
		newsService.unbindTags(newsId);
		newsService.bindAuthor(newsId, newsVO.getAuthor().getId());
		if (!newsVO.getTags().isEmpty()) {
			newsService.bindTags(newsId, newsVO.getTags());
		}
		newsService.update(news);
	}

	@Override
	public void remove(News news) throws ServiceException {
		Long newsId = news.getId();
		newsService.unbindAuthor(newsId);
		newsService.unbindTags(newsId);
		commentService.removeCommentFromNews(newsId);
		newsService.remove(newsId);
	}

	@Override
	public NewsVO findByNewsId(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsService.findById(newsId));
		newsVO.setAuthor(authorService.findByNewsId(newsId));
		newsVO.setTags(tagService.findByNewsId(newsId));
		newsVO.setComments(commentService.findByNewsId(newsId));
		return newsVO;
	}

	@Override
	public List<NewsVO> findAll(List<News> news) throws ServiceException {
		List<NewsVO> resultList = null;
		if (news != null) {
			resultList = new ArrayList<NewsVO>();
			for (News newsObj : news) {
				NewsVO newsVO = findByNewsId(newsObj.getId());
				resultList.add(newsVO);
			}
		}
		return resultList;
	}

	public NewsService getNewsService() {
		return newsService;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public AuthorService getAuthorService() {
		return authorService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}

	public TagService getTagService() {
		return tagService;
	}

	public void setTagService(TagService tagService) {
		this.tagService = tagService;
	}

	public CommentService getCommentService() {
		return commentService;
	}

	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
}
