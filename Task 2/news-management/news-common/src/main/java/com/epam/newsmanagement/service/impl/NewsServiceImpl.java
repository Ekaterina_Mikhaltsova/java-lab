package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.exception.ServiceException;
import com.epam.newsmanagement.utils.SearchCriteria;

/**
 * Class implementation of {@link NewsService} interface.
 * <p>
 * Implements all operations defined in {@link NewsService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class NewsServiceImpl implements NewsService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private NewsDAO newsDAO;

	@Override
	public Long add(News news) throws ServiceException {
		try {
			return newsDAO.add(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding news", e);
		}
	}

	@Override
	public void bindTags(Long newsId, List<Tag> tags) throws ServiceException {
		try {
			newsDAO.bindTags(newsId, tags);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding tags to news", e);
		}
	}

	@Override
	public void unbindTags(Long newsId) throws ServiceException {
		try {
			newsDAO.unbindTags(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing tags from news", e);
		}
	}

	@Override
	public void bindAuthor(Long newsId, Long authorId) throws ServiceException {
		try {
			newsDAO.bindAuthor(newsId, authorId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding author to news", e);
		}
	}

	@Override
	public void unbindAuthor(Long newsId) throws ServiceException {
		try {
			newsDAO.unbindAuthor(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing author from news", e);
		}
	}

	@Override
	public News findById(Long newsId) throws ServiceException {
		try {
			return newsDAO.findById(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding news by ID", e);
		}
	}
	
	@Override
	public void update(News news) throws ServiceException {
		try {
			newsDAO.update(news);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while updating news", e);
		}
	}
	
	@Override
	public void remove(Long newsId) throws ServiceException {
		try {
			newsDAO.remove(newsId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing news", e);
		}
	}
	
	@Override
	public int countNews(SearchCriteria searchCriteria) throws ServiceException {
		try {
			return newsDAO.countNews(searchCriteria);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while counting news", e);
		}
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int pageId, int newsOnPage)	throws ServiceException {
		try {
			int end = newsOnPage;
			int start = newsOnPage - 1;
			return newsDAO.search(searchCriteria, pageId, start, end);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while reading news", e);
		}
	}

	public NewsDAO getNewsDAO() {
		return newsDAO;
	}

	public void setNewsDAO(NewsDAO newsDAO) {
		this.newsDAO = newsDAO;
	}
}
