package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.List;

/**
 * Class represents News Value Object model.
 *
 * @author Kate
 */
public class NewsVO implements Serializable {

	private static final long serialVersionUID = 1L;
		
	/**
	 * {@link News} object.
	 */
	private News news;
	
	/**
	 * {@link Author} object.
	 */
	private Author author;
	
	/**
	 * {@link List} of {@link Tag} objects.
	 */
	private List<Tag> tags;
	
	/**
	 * {@link List} of {@link Comment} objects.
	 */
	private List<Comment> comments;
	
	/**
	 * Constructs {@link NewsVO} object with default field's values.
	 */
	public NewsVO() {
	}
	
	/**
	 * Constructs {@link NewsVO} object with given field's values.
	 */
	public NewsVO(News news, Author author, List<Tag> tags, List<Comment> comments) {
		this.news = news;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}

	public News getNews() {
		return news;
	}

	public void setNews(News news) {
		this.news = news;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
}
