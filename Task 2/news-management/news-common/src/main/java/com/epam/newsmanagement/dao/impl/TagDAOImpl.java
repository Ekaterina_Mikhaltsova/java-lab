package com.epam.newsmanagement.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.utils.DBUtil;
import com.epam.newsmanagement.utils.exception.DBUtilException;

/**
 * Class implementation of {@link TagDAO} interface.
 * <p>
 * Implements all operations on TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagDAOImpl implements TagDAO {

	private static final String SQL_INSERT_TAG = "INSERT into tags (tag_id,tag_name) "
			+ "VALUES (tags_seq.nextval,?)";
	
	private static final String SQL_FIND_TAGS_BY_NEWS_ID = "SELECT tags.tag_id, tags.tag_name FROM tags "
			+ "INNER JOIN news_tags ON tags.tag_id = news_tags.nt_tag_id WHERE nt_news_id=?";
	
	private static final String SQL_FIND_ALL_TAGS = "SELECT tag_id, tag_name FROM tags";

	private static final String SQL_UPDATE_TAG = "UPDATE tags SET tag_name=? WHERE tag_id=?";
	
	private static final String SQL_DELETE_TAG = "DELETE FROM tags WHERE tag_id=?";
	
	private static final String SQL_DELETE_TAG_FROM_NEWS = "DELETE FROM news_tags WHERE nt_tag_id=?";
	
	private DataSource dataSource;
	
	@Override
	public void add(Tag tag) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_INSERT_TAG);
			ps.setString(1, tag.getName());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while inserting new tag", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public List<Tag> findByNewsId(Long newsId) throws DAOException {
		List<Tag> tags = new ArrayList<Tag>();
		PreparedStatement ps = null;
		ResultSet rs = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_FIND_TAGS_BY_NEWS_ID);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for tags", e);
		} finally {
			try {
				DBUtil.close(rs, ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return tags;
	}	

	@Override
	public List<Tag> findAll() throws DAOException {
		Statement st = null;
		ResultSet rs = null;
		Connection con = null;
		List<Tag> tags = new ArrayList<Tag>();
		try {
			con = dataSource.getConnection();
			st = con.createStatement();
			rs = st.executeQuery(SQL_FIND_ALL_TAGS);
			while (rs.next()) {
				Tag tag = new Tag();
				tag.setId(rs.getLong("tag_id"));
				tag.setName(rs.getString("tag_name"));
				tags.add(tag);
			}
		} catch (SQLException e) {
			throw new DAOException("Error while searching for tags", e);
		} finally {
			try {
				DBUtil.close(rs, st, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
		return tags;
	}

	@Override
	public void update(Tag tag) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = dataSource.getConnection();
			ps = con.prepareStatement(SQL_UPDATE_TAG);
			ps.setString(1, tag.getName());
			ps.setLong(2, tag.getId());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while updating tag", e);
		} finally {
			try {
				DBUtil.close(ps, con);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void delete(Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_TAG);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting tag", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	@Override
	public void deleteFromNews(Long tagId) throws DAOException {
		Connection con = null;
		PreparedStatement ps = null;
		try {
			con = DataSourceUtils.getConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_TAG_FROM_NEWS);
			ps.setLong(1, tagId);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException("Error while deleting tag from news", e);
		} finally {
			try {
				DBUtil.close(ps, con, dataSource);
			} catch (DBUtilException e) {
				throw new DAOException("Error while closing resources", e);
			}
		}
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
}
