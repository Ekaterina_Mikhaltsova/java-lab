package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.dao.exception.DAOException;
import com.epam.newsmanagement.entity.Comment;

/**
 * Interface that represents a contract for a DAO for the {@link Comment} model.
 * <p>
 * Contains methods for interaction with database COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface CommentDAO {

	/**
     * Method adds given comment to the COMMENTS table.
     * 
     * @param comment the comment to be added
     * 
     * @throws DAOException
     */
	void add(Comment comment) throws DAOException;
	
	/**
     * Method removes comment by given {@link Long} news id.
     * 
     * @param commentId id of comment to be removed
     * 
     * @throws DAOException
     */
	void remove(Long commentId) throws DAOException;
	
	/**
     * Method removes all comments from given news.
     * 
     * @param newsId id of news
     * 
     * @throws DAOException
     */
	void removeCommentFromNews(Long newsId) throws DAOException;
	
	/**
     * Method finds comments by given {@link Long} news id.
     * 
     * @param newsId id of news
     * 
     * @return found {@link List} of {@link Comment} comments
     * 
     * @throws DAOException
     */
	List<Comment> findByNewsId(Long newsId) throws DAOException;
}
