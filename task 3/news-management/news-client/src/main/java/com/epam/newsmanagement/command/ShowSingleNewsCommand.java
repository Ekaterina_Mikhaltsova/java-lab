package com.epam.newsmanagement.command;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;

import com.epam.newsmanagement.controller.ContextLoader;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.NewsService;

public class ShowSingleNewsCommand implements ICommand {

	private NewsService newsService;
	
	private MessageSource messageSource;
	
	public ShowSingleNewsCommand() {
		newsService = ContextLoader.getInstance().getCommonService().getNewsService();
		messageSource = ContextLoader.getInstance().getMessageSource();
	}
	
	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		String page = messageSource.getMessage("single.news.page", null, Locale.US);
		Long id = Long.parseLong(request.getParameter("id"));
		News news = newsService.findById(id);
		request.setAttribute("news", news);
		return page;
	}
}
