package com.epam.newsmanagement.command;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;

import com.epam.newsmanagement.controller.ContextLoader;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;

public class AddCommentCommand implements ICommand {

	private CommentService commentService;
	
	private NewsService newsService;
	
	private MessageSource messageSource;

	public AddCommentCommand() {
		commentService = ContextLoader.getInstance().getCommonService().getCommentService();
		newsService = ContextLoader.getInstance().getCommonService().getNewsService();
		messageSource = ContextLoader.getInstance().getMessageSource();
	}

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		String page = messageSource.getMessage("single.news.page", null, Locale.US);
		Long newsId = Long.parseLong(request.getParameter("newsId"));
		String text = request.getParameter("commentText");
		News news;
		if (text.length() <= 30) {
			Comment comment = new Comment();
			comment.setText(text);
			commentService.add(comment);

		}
		news = newsService.findById(newsId);
		request.setAttribute("news", news);
		return page;
	}
}
