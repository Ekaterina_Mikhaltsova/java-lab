package com.epam.newsmanagement.controller;

import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.epam.newsmanagement.service.impl.CommonService;

public class ContextLoader {

	public static ApplicationContext ac;
	
	private static ContextLoader instance;
	
	private static CommonService commonService;
	
	private static MessageSource messageSource;

	public synchronized static ContextLoader getInstance() {
		if (instance == null) {
			instance = new ContextLoader();
		}
		return instance;
	}

	public void loadContext() {
		ac = new ClassPathXmlApplicationContext("classpath:spring-client.xml");
		commonService = (CommonService) ac.getBean("commonService");
		messageSource = (MessageSource) ac.getBean("messageSource");
	}

	public CommonService getCommonService() {
		return commonService;
	}
	
	public MessageSource getMessageSource() {
		return messageSource;
	}
}
