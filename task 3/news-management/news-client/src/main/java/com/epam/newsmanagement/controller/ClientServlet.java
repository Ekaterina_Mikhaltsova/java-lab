package com.epam.newsmanagement.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.exception.ServiceException;

@WebServlet(name = "ClientServlet", urlPatterns = {"/controller"})
public class ClientServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	public void init() {
		ContextLoader.getInstance().loadContext();
	}
	
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {	
		HttpSession session = request.getSession(true);
		String command  = request.getParameter("command");
		ICommand iCommand = RequestHelper.getInstance().getCommand(command);
		String page = null;
		try {
			page = iCommand.execute(request, session);
		} catch (ServiceException e) {
			page = "/WEB-INF/jsp/error.jsp";
		}
		if (page == null) {
			page = "/WEB-INF/jsp/error.jsp";
		}
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
}
