package com.epam.newsmanagement.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.context.MessageSource;

import com.epam.newsmanagement.controller.ContextLoader;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;
import com.epam.newsmanagement.utils.SearchCriteria;

public class FilterNewsCommand implements ICommand {

	private AuthorService authorService;
	
	private TagService tagService;
	
	private NewsService newsService;

	private MessageSource messageSource;

	private static final int NEWS_ON_PAGE = 3;
	
	public FilterNewsCommand() {
		authorService = ContextLoader.getInstance().getCommonService().getAuthorService();
		tagService = ContextLoader.getInstance().getCommonService().getTagService();
		newsService = ContextLoader.getInstance().getCommonService().getNewsService();
		messageSource = ContextLoader.getInstance().getMessageSource();
	}

	@Override
	public String execute(HttpServletRequest request, HttpSession session)
			throws ServiceException {
		String page = messageSource.getMessage("main.page", null, Locale.US);
		List<News> news;
		List<Author> authors;
		List<Tag> tags;
		int pageQuantity = 1;
		Long authorId = null;
		List<Long> tagIds = new ArrayList<Long>();
		int pageNumber = 1;
		int currentPage = 1;
		if (request.getParameter("author") != null) {
			authorId = Long.parseLong(request.getParameter("author"));
		}
		if (request.getParameterValues("tags") != null) {
			String[] tagsIds = request.getParameterValues("tags");
			for (String tagId : tagsIds) {
				tagIds.add(Long.parseLong(tagId));
			}
		}
		if (request.getParameter("pageNumber") != null) {
			if (session.getAttribute("authorId") != null) {
				authorId = (Long) session.getAttribute("authorId");
			}
			if (session.getAttribute("tagIds") != null) {
				tagIds = (List<Long>) session.getAttribute("tagIds");
			}
			pageNumber = Integer.parseInt(request.getParameter("pageNumber"));
			currentPage = pageNumber;
		}

		int newsQuantity = newsService.countNews(buildCriteria(authorId,
				tagIds, session));
		if (newsQuantity % NEWS_ON_PAGE == 0) {
			pageQuantity = newsQuantity / NEWS_ON_PAGE;
		} else {
			pageQuantity = newsQuantity / NEWS_ON_PAGE + 1;
		}
		news = newsService.search(buildCriteria(authorId, tagIds, session),
				pageNumber, NEWS_ON_PAGE);
		authors = authorService.findAll();
		tags = tagService.findAll();
		if (news == null) {
			session.setAttribute("news", null);
		} else {
			session.setAttribute("news", news);
			request.setAttribute("news", news);
		}
		request.setAttribute("currentPage", currentPage);
		request.setAttribute("authors", authors);
		request.setAttribute("tags", tags);
		request.setAttribute("pageQuantity", pageQuantity);
		return page;
	}

	private SearchCriteria buildCriteria(Long authorId, List<Long> tagIds,
			HttpSession session) {
		SearchCriteria searchCriteria = new SearchCriteria();
		if (authorId != null && authorId > 0L) {
			searchCriteria.setAuthorId(authorId);
			session.setAttribute("authorId", authorId);
		}
		if (!tagIds.isEmpty()) {
			searchCriteria.setTagIds(tagIds);
			session.setAttribute("tagIds", tagIds);
		}
		return searchCriteria;
	}

	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public void setNewsService(NewsService newsService) {
		this.newsService = newsService;
	}

	public void setAuthorService(AuthorService authorService) {
		this.authorService = authorService;
	}
}
