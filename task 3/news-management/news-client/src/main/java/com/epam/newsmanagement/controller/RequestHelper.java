package com.epam.newsmanagement.controller;

import java.util.HashMap;
import java.util.Map;

import com.epam.newsmanagement.command.AddCommentCommand;
import com.epam.newsmanagement.command.FilterNewsCommand;
import com.epam.newsmanagement.command.ICommand;
import com.epam.newsmanagement.command.ResetCommand;
import com.epam.newsmanagement.command.ShowSingleNewsCommand;

public class RequestHelper {

	private static Map<String, ICommand> commands = new HashMap<String, ICommand>();
	
	private static RequestHelper instance;

	public synchronized static RequestHelper getInstance() {
		if (instance == null) {
			instance = new RequestHelper();
		}
		return instance;
	}

	public RequestHelper() {
		commands.put("filter", new FilterNewsCommand());
		commands.put("showSingleNews", new ShowSingleNewsCommand());
		commands.put("addComment", new AddCommentCommand());
		commands.put("reset", new ResetCommand());
	}

	public ICommand getCommand(String command) {
		return commands.get(command);
	}
}
