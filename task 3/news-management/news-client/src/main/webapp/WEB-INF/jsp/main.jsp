<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<c:set var="lang" value="${not empty param.lang ? param.lang : not empty lang ? lang : pageContext.request.locale}" scope="session" />
<fmt:setLocale value="${lang}" />
<fmt:setBundle basename="content" />

<html>

<head>
	<link rel="stylesheet" href="<c:url value="/resources/css/style.css"></c:url>" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/css/ui.dropdownchecklist.standalone.css"></c:url>" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/css/ui.dropdownchecklist.themeroller.css"></c:url>" type="text/css" />
	<link rel="stylesheet" href="<c:url value="/resources/css/simplePagination.css"></c:url>" type="text/css" />
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-1.6.1.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery-ui-1.8.13.custom.min.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/ui.dropdownchecklist.js'/>"></script>
    <script type="text/javascript" src="<c:url value='/resources/js/jquery.simplePagination.js'/>"></script>
	
	<title>Main</title>
	
	<script type="text/javascript">
		$(function() {
			$("#pag").pagination({
				pages : "${pageQuantity}",
				hrefTextPrefix : "controller?command=filter&pageNumber=",
				cssStyle : "light-theme",
				currentPage: "${currentPage}"
			});
		});
	</script>
	
</head>

<body>
	<jsp:include page="header.jsp" />
	<div align="right">
		<form>
			<input type="hidden" name="command" value="filter" /> 
			<input type="hidden" name="pageNumber" value="${currentPage}"> 
			<select id=lang name="lang" onchange="submit()">			
				<option value="ru" ${lang == 'ru' ? 'selected' : ''}>Russian</option>
				<option value="en" ${lang == 'en' ? 'selected' : ''}>English</option>
			</select>
		</form><hr>
	</div>
	<div class="content">
		<div class="filter" align="center">
			<table border="0">
				<tr>
					<td>
						<form name="filter" method="POST" action="controller">
							<select name="author">
								<c:if test="${authorId != null}">
									<option value="-1"><fmt:message key="select.author" /></option>
								</c:if>
								<c:if test="${authorId == null}">
									<option selected="selected" value="-1"><fmt:message key="select.author" /></option>
								</c:if>
								<c:forEach items="${authors}" var="author">
									<c:if test="${authorId != author.id}">
										<option value="${author.id}">
											<c:out value="${author.name}" />
										</option>
									</c:if>
									<c:if test="${authorId == author.id}">
										<option selected="selected" value="${author.id}">
											<c:out value="${author.name}" />
										</option>
									</c:if>
								</c:forEach>
							</select> 							
							<select name="tags" multiple id="multi-dropbox">
								<c:forEach items="${tags}" var="tag">
									<c:set var="contains" value="false" />
									<c:forEach var="item" items="${tagIds}">
										<c:if test="${item == tag.id}">
											<c:set var="contains" value="true" />
										</c:if>
									</c:forEach>
									<c:if test="${contains == true}">
										<option selected value="${tag.id}">
											<c:out value="${tag.name}" />
										</option>
									</c:if>
									<c:if test="${contains != true}">
										<option value="${tag.id}">
											<c:out value="${tag.name}" />
										</option>
									</c:if>
								</c:forEach>
							</select> 
							<input type="hidden" name="command" value="filter" /> 
							<input type="submit" value="<fmt:message key="filter.button" />" />
						</form>
					</td>
					<td>
						<form name="reset" method="POST" action="controller">
							<input type="hidden" name="command" value="reset" /> 
							<input type="submit" value="<fmt:message key="reset.button" />" />
						</form>
					</td>
				</tr>
			</table>
		</div>
		<br>
 		<script type="text/javascript">
 			$("#multi-dropbox").dropdownchecklist({
 				emptyText : "<fmt:message key="select.tags" />",
 				width : 120
 			});
 		</script>
		<div align="center"><c:if test="${news == null}"><h3><fmt:message key="no.news.message" /></h3></c:if></div>
   		<c:if test="${news != null}">
			<c:forEach var="news" items="${news}">
				<div class="news-title">${news.title}</div>
				<div class="simple">(<fmt:message key="by.message" /> ${news.author.name})</div>
				<div class="date">
					<fmt:formatDate pattern="dd/MM/yyyy" value="${news.modificationDate}" />
				</div><br><br>
				${news.shortText}<br>
				<div class="tag-comment" align="right">
					<div class="tags">
						<c:forEach var="tag" items="${news.tags}" varStatus="status">
							${tag.name}
							<c:if test="${!status.last}">,</c:if>
						</c:forEach>
					</div>
					<div class="comments"><fmt:message key="comments.message" />(${fn:length(news.comments)})</div>
					<a href="controller?command=showSingleNews&id=${news.id}"><fmt:message key="view.button" /></a>
				</div><br><br>
			</c:forEach>
		</c:if>
	</div>
	<c:set var="currentPage" value="${currentPage}" />
	<div align="center">
		<div id="pag"></div>
	</div>
	<jsp:include page="footer.jsp" />
</body>

</html>