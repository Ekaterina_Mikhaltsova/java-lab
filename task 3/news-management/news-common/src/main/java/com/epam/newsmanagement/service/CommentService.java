package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Comment} model.
 * <p>
 * Contains methods for interaction with {@link CommentDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface CommentService {

	/**
     * Method adds given comment.
     * 
     * @param comment the comment to be added
     * 
     * @throws ServiceException
     */
	 void add(Comment comment) throws ServiceException;
	 
	/**
	 * Method removes comment by given {@link Long} news id.
	 * 
	 * @param commentId id of comment to be removed
	 * 
	 * @return {@link Long} id of created comment
	 * 
	 * @throws ServiceException
	 */
	 void remove(Long commentId) throws ServiceException;
}
