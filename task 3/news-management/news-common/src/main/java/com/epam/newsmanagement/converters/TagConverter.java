package com.epam.newsmanagement.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

public class TagConverter implements Converter<String, Tag> {
	@Autowired
	private TagService tagService;

	@Override
	public Tag convert(String newsId) {
		try {
			return tagService.findById(Long.parseLong(newsId));
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		return null;
	}
}
