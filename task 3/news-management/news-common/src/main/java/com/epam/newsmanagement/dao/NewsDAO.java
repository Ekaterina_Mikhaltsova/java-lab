package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.SearchCriteria;

/**
 * Interface that represents a contract for a DAO for the {@link News} model.
 * <p>
 * Contains methods for interaction with database NEWS table and linked tables NEWS_AUTHORS and NEWS_TAGS.
 * </p>
 * 
 * @author Kate
 *
 */
public interface NewsDAO {

	/**
     * Method adds given news to the NEWS table.
     * 
     * @param news the news to be added
     * 
     * @return {@link Long} id of created news
     * 
     * @throws DAOException
     */
	Long add(News news) throws DAOException;
	
	/**
     * Method finds news by given {@link Long} news id.
     * 
     * @param newsId id of news to be found
     * 
     * @return found {@link News} news
     * 
     * @throws DAOException
     */
	News findById(Long newsId) throws DAOException;
	
	/**
     * Method updates news by given {@link News} news.
     * 
     * @param news the news with updated information and same id
     * 
     * @throws DAOException
     */
	void update(News news) throws DAOException;
	
	/**
     * Method removes news by given {@link Long} news id.
     * 
     * @param newsId id of news to be removed
     * 
     * @throws DAOException
     */
	void remove(Long newsId) throws DAOException;
	
	/**
     * Method counts all news fitting the criteria.
     * 
     * @param searchCriteria criteria of what news must be found
     * 
     * @return news quantity
     * 
     * @throws DAOException
     */
	int countNews(SearchCriteria searchCriteria) throws DAOException;
	
	/**
     * Method finds news according to {@link SearchCriteria} criteria and sorts by most commented news
     * and modification date.
     * 
     * @param searchCriteria criteria of searching for news
     * @param pageId id of page
     * @param newsOnPage number of news on page
     * 
     * @return {@link List} of {@link News} objects
     * 
     * @throws DAOException
     */
	List<News> search(SearchCriteria searchCriteria, int pageId, int newsOnPage) throws DAOException;
}
