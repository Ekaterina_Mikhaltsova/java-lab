package com.epam.newsmanagement.utils;

import javax.persistence.EntityManager;

import com.epam.newsmanagement.exception.DAOException;

public class JPADAOUtil {

	public static void closeManager(EntityManager manager) throws DAOException {
		if ((manager != null) && manager.isOpen()) {
			try {
				manager.close();
			} catch (IllegalStateException e) {
				throw new DAOException(e);
			}
		}
	}
}
