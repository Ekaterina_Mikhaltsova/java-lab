package com.epam.newsmanagement.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.TagService;

/**
 * Class implementation of {@link TagService} interface.
 * <p>
 * Implements all operations defined in {@link TagService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
@Service
public class TagServiceImpl implements TagService {
	
	private final Logger logger = Logger.getLogger(this.getClass());
	
	private TagDAO tagDAO;
	
	@Override
	public void add(Tag tag) throws ServiceException {
		try {
			tagDAO.add(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while addding tag", e);
		}
	}

	@Override
	public List<Tag> findAll() throws ServiceException {
		try {
			return tagDAO.findAll();
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while finding all tags", e);
		}
	}

	@Override
	public void update(Tag tag) throws ServiceException {
		try {
			tagDAO.update(tag);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while updating tag", e);
		}
	}
	
	@Override
	public void delete(Long tagId) throws ServiceException {
		try {
			tagDAO.delete(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while deleting tag", e);
		}
	}

	@Override
	public Tag findById(Long tagId) throws ServiceException {
		try {
			return tagDAO.findById(tagId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while deleting tag", e);
		}
	}

	public void setTagDAO(TagDAO tagDAO) {
		this.tagDAO = tagDAO;
	}
}
