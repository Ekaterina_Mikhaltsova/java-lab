package com.epam.newsmanagement.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;

/**
 * Class implementation of {@link CommentService} interface.
 * <p>
 * Implements all operations defined in {@link CommentService} interface.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentServiceImpl implements CommentService {
	
	private final Logger logger = Logger.getLogger(this.getClass());

	@Autowired
	private CommentDAO commentDAO;

	@Override
	public void add(Comment comment) throws ServiceException {
		try {
			commentDAO.add(comment);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while adding comment", e);
		}
	}

	@Override
	public void remove(Long commentId) throws ServiceException {
		try {
			commentDAO.remove(commentId);
		} catch (DAOException e) {
			logger.error(e.getMessage());
			throw new ServiceException("Error while removing comment", e);
		}
	}

	public CommentDAO getCommentDAO() {
		return commentDAO;
	}

	public void setCommentDAO(CommentDAO commentDAO) {
		this.commentDAO = commentDAO;
	}
}
