package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Tag} model.
 * <p>
 * Contains methods for interaction with {@link TagDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface TagService {

	/**
     * Method adds given tag.
     * 
     * @param tag tag to be added
     * 
     * @return found {@link List} of {@link Tag} tags
     * 
     * @throws ServiceException
     */
	void add(Tag tag) throws ServiceException;
	
	/**
     * Method finds tags by given {@link Long} tag id.
     * 
     * @param tagId id of news
     * 
     * @return found {@link Tag} tag
     * 
     * @throws ServiceException
     */
	Tag findById(Long tagId) throws ServiceException;
	
	/**
     * Method finds all tags.
     * 
     * @return found {@link List} of {@link Tag} tags
     * 
     * @throws ServiceException
     */
	List<Tag> findAll() throws ServiceException;
	
	/**
     * Method updates given tag.
     * 
     * @param tag tag to update
     * 
     * @throws ServiceException
     */
	void update(Tag tag) throws ServiceException;
	
	/**
     * Method deletes given tag.
     * 
     * @param tagId tag to delete
     * 
     * @throws ServiceException
     */
	void delete(Long tagId) throws ServiceException;
}
