package com.epam.newsmanagement.dao.eclipselink;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;

import static com.epam.newsmanagement.utils.JPADAOUtil.*;

/**
 * Class implementation of {@link TagDAO} interface.
 * <p>
 * Implements all operations on TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagDAOImpl implements TagDAO {

	private EntityManagerFactory managerFactory;

	public TagDAOImpl() {
		managerFactory = JPAEntityManager.getEntityManagerFactory();
	}

	@Override
	public void add(Tag tag) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			tag.setId(null);
			manager.persist(tag);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public Tag findById(Long tagId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		Tag result = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			result = manager.find(Tag.class, tagId);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return result;
	}

	@Override
	public List<Tag> findAll() throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		List<Tag> tags = new ArrayList<Tag>();
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Query query = manager
					.createQuery("SELECT t FROM Tag t ORDER BY t.name");
			tags = query.getResultList();
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return tags;
	}

	@Override
	public void update(Tag tag) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Tag tempTag = manager.find(Tag.class, tag.getId());
			tempTag.setName(tag.getName());
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public void delete(Long tagId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Tag entity = manager.find(Tag.class, tagId);
			manager.remove(entity);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}
}
