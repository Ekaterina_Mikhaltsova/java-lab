package com.epam.newsmanagement.dao.hibernate;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.epam.newsmanagement.dao.TagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;

/**
 * Class implementation of {@link TagDAO} interface.
 * <p>
 * Implements all operations on TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class TagDAOImpl implements TagDAO {
	
	@Override
	public void add(Tag tag) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(tag);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}

	@Override
	public List<Tag> findAll() throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<Tag> tags = null;
		try {
			transaction = session.beginTransaction();
			tags = session.createCriteria(Tag.class).list();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return tags;
	}

	@Override
	public void update(Tag tag) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(tag);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}

	@Override
	public void delete(Long tagId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Tag tag = (Tag) session.load(Tag.class, tagId);
            session.delete(tag);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}
		
	@Override
	public Tag findById(Long tagId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Tag tag = null;
		try {
			tag = (Tag) session.get(Tag.class, tagId);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return tag;
	}

	private void closeSession(Session session){
        if (session.isOpen()) {
            session.close();
        }
    }
}
