package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface that represents a contract for a DAO for the {@link Comment} model.
 * <p>
 * Contains methods for interaction with database COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface CommentDAO {

	/**
     * Method adds given comment to the COMMENTS table.
     * 
     * @param comment the comment to be added
     * 
     * @throws DAOException
     */
	void add(Comment comment) throws DAOException;
	
	/**
     * Method removes comment by given {@link Long} news id.
     * 
     * @param commentId id of comment to be removed
     * 
     * @throws DAOException
     */
	void remove(Long commentId) throws DAOException;
}
