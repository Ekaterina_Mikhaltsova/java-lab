package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Class represents Author model.
 *
 * @author Kate
 */
@Entity
@Table(name = "AUTHORS")
public class Author implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Author id.
	 */
	@Id
	@GeneratedValue(generator = "authors_seq")
	@SequenceGenerator(name = "authors_seq", sequenceName = "AUTHORS_SEQ", allocationSize = 1)
	@Column(name = "AUTHOR_ID", nullable = false, unique = true)
	private Long id;

	/**
	 * Author name.
	 */
	@Size(min = 1, max = 30, message = "{Size}")
	@Column(name = "AUTHOR_NAME")
	private String name;

	/**
	 * Date of author's expiration.
	 */
	@Column(name = "EXPIRED")
	private Date expired;

	/**
	 * Constructs {@link Author} object with default field's values.
	 */
	public Author() {
	}

	/**
	 * Constructs {@link Author} object with given field's values.
	 */
	public Author(Long id, String name, Date expired) {
		this.id = id;
		this.name = name;
		this.expired = expired;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Author [id=" + id + ", name=" + name + ", expired=" + expired
				+ "]";
	}
}
