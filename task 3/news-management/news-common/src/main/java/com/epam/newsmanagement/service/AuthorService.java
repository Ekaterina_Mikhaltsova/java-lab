package com.epam.newsmanagement.service;

import java.util.List;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

/**
 * Interface that represents a service for the {@link Author} model.
 * <p>
 * Contains methods for interaction with {@link AuthorDAO} methods.
 * </p>
 * 
 * @author Kate
 *
 */
public interface AuthorService {
	
	/**
     * Method adds given author.
     * 
     * @param author the author to be added
     * 
     * @throws ServiceException
     */
	void add(Author author) throws ServiceException;
	
	/**
     * Method finds author by given {@link Long} author id.
     * 
     * @param authorId id of news
     * 
     * @return found {@link Author} author
     * 
     * @throws ServiceException
     */
	Author findById(Long authorId) throws ServiceException;
	
	/**
     * Method updates given author.
     * 
     * @param author author to be updated
     * 
     * @throws ServiceException
     */
	void update(Author author) throws ServiceException;
	
	/**
     * Method makes given author expired.
     * 
     * @param authorId id of author to be made expired
     * 
     * @throws ServiceException
     */
	void expired(Long authorId) throws ServiceException;
	
	/**
     * Method finds all authors.
     * 
     * @return found {@link List} of {@link Author} authors
     * 
     * @throws ServiceException
     */
	List<Author> findAll() throws ServiceException;
}
