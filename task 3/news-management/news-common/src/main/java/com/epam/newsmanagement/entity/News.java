package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Class represents News model.
 *
 * @author Kate
 */
@Entity
@Table(name = "NEWS")
public class News implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * News id.
	 */
	@Id
	@GeneratedValue(generator = "news_seq")
	@SequenceGenerator(name = "news_seq", sequenceName = "NEWS_SEQ", allocationSize = 1)
	@Column(name = "NEWS_ID", nullable = false, unique = true)
	private Long id;

	/**
	 * News title.
	 */
	@Size(min = 1, max = 30, message = "{Size}")
	@Column(name = "TITLE")
	private String title;

	/**
	 * News short text.
	 */
	@Size(min = 1, max = 100, message = "{Size}")
	@Column(name = "SHORT_TEXT")
	private String shortText;

	/**
	 * News full text.
	 */
	@Size(min = 1, max = 2000, message = "{Size}")
	@Column(name = "FULL_TEXT")
	private String fullText;

	/**
	 * News creation date.
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/**
	 * News modification date (changes after updating news).
	 */
	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	/**
	 * News author
	 */
	@ManyToOne(cascade = CascadeType.REMOVE, fetch = FetchType.EAGER)
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "NA_NEWS_ID", referencedColumnName = "NEWS_ID") }, 
	inverseJoinColumns = { @JoinColumn(name = "NA_AUTHOR_ID", referencedColumnName = "AUTHOR_ID") })
	private Author author;

	/**
	 * News tags
	 */
	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NT_NEWS_ID", referencedColumnName = "NEWS_ID") }, 
	inverseJoinColumns = { @JoinColumn(name = "NT_TAG_ID", referencedColumnName = "TAG_ID") })
	private List<Tag> tags;

	/**
	 * News comments
	 */
	@OneToMany(targetEntity = Comment.class, cascade = CascadeType.REMOVE)
	@JoinColumn(name = "C_NEWS_ID", referencedColumnName = "NEWS_ID", updatable = false)
	@OrderBy("creationDate DESC")
	private List<Comment> comments;

	/**
	 * Constructs {@link News} object with default field's values.
	 */
	public News() {
		comments = new ArrayList<Comment>();
		tags = new ArrayList<Tag>();
	}

	/**
	 * Constructs {@link News} object with given field's values.
	 */
	public News(Long id, String title, String shortText, String fullText,
			Date creationDate, Date modificationDate, Author author,
			List<Tag> tags, List<Comment> comments) {
		super();
		this.id = id;
		this.title = title;
		this.shortText = shortText;
		this.fullText = fullText;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
		this.author = author;
		this.tags = tags;
		this.comments = comments;
	}	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Tag> getTags() {
		return tags;
	}

	public void setTags(List<Tag> tags) {
		this.tags = tags;
	}

	public List<Comment> getComments() {
		return comments;
	}

	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((author == null) ? 0 : author.hashCode());
		result = prime * result
				+ ((comments == null) ? 0 : comments.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result + ((tags == null) ? 0 : tags.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (author == null) {
			if (other.author != null)
				return false;
		} else if (!author.equals(other.author))
			return false;
		if (comments == null) {
			if (other.comments != null)
				return false;
		} else if (!comments.equals(other.comments))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tags == null) {
			if (other.tags != null)
				return false;
		} else if (!tags.equals(other.tags))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", title=" + title + ", shortText="
				+ shortText + ", fullText=" + fullText + ", creationDate="
				+ creationDate + ", modificationDate=" + modificationDate
				+ ", author=" + author + ", tags=" + tags + ", comments="
				+ comments + "]";
	}
}
