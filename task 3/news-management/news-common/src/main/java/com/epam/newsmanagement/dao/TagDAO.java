package com.epam.newsmanagement.dao;

import java.util.List;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

/**
 * Interface that represents a contract for a DAO for the {@link Tag} model.
 * <p>
 * Contains methods for interaction with database TAGS table.
 * </p>
 * 
 * @author Kate
 *
 */
public interface TagDAO {

	/**
     * Method adds given tag.
     * 
     * @param tag tag to add
     * 
     * @throws DAOException
     */
	void add(Tag tag) throws DAOException;
	
	/**
     * Method finds tags by given {@link Long} tag id.
     * 
     * @param tagId id of news
     * 
     * @return found {@link Tag} tag
     * 
     * @throws DAOException
     */
	Tag findById(Long tagId) throws DAOException;
	
	/**
     * Method finds all tags.
     * 
     * @return found {@link List} of {@link Tag} tags
     * 
     * @throws DAOException
     */
	List<Tag> findAll() throws DAOException;
	
	/**
     * Method updates chosen tag.
     * 
     * @param tag tag to update
     * 
     * @throws DAOException
     */
	void update(Tag tag) throws DAOException;
	
	/**
     * Method deletes chosen tag.
     * 
     * @param tagId tag to delete
     * 
     * @throws DAOException
     */
	void delete(Long tagId) throws DAOException;
}
