package com.epam.newsmanagement.exception;

/**
 * Class defines DAO layer exception.
 * <p>
 * This exception is thrown in DAO methods if any problem with interacting
 * with database occurs.
 * </p>
 * 
 * @author Kate
 *
 */
public class DAOException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
     * Constructor that gets {@link String} message.
     * 
     * @param message defines why the exception was thrown.
     */
	public DAOException(String message) {
		super(message);
	}
		
	/**
     * Constructor that gets {@link Throwable} cause.
     * 
     * @param cause defines the underlying cause.
     */
	public DAOException(Throwable cause) {
		super(cause);
	}
	
	/**
     * Constructor that gets {@link String} message and {@link Throwable} cause.
     * 
     * @param message defines why the exception was thrown.
     * @param cause defines the underlying cause.
     */
	public DAOException(String message, Throwable cause) {
		super(message, cause);
	}
}
