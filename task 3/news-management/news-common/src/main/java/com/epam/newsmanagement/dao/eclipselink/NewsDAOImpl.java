package com.epam.newsmanagement.dao.eclipselink;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;
import com.epam.newsmanagement.utils.SearchCriteria;

import static com.epam.newsmanagement.utils.JPADAOUtil.*;

/**
 * Class implementation of {@link NewsDAO} interface.
 * <p>
 * Implements all operations on NEWS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class NewsDAOImpl implements NewsDAO {

	private EntityManagerFactory managerFactory;

	public NewsDAOImpl() {
		managerFactory = JPAEntityManager.getEntityManagerFactory();
	}

	@Override
	public Long add(News news) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			news.setId(null);
			List<Tag> tags = new ArrayList<Tag>();
			if (news.getTags() != null) {
				for (Tag tag : news.getTags()) {
					tags.add(manager.find(Tag.class, tag.getId()));
				}
			}
			news.setTags(tags);
			news.setModificationDate(new Date(System.currentTimeMillis()));
			manager.persist(news);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return news.getId();
	}

	@Override
	public News findById(Long newsId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction = null;
		News news = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			news = manager.find(News.class, newsId);
			news.getComments().size();
			news.getTags().size();
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return news;
	}

	@Override
	public void update(News news) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			news.setModificationDate(new Date(System.currentTimeMillis()));
			News temp = findById(news.getId());
			news.setComments(temp.getComments());
			manager.merge(news);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public void remove(Long newsId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			News entity = manager.find(News.class, newsId);
			entity.setAuthor(null);
			entity.setTags(null);
			manager.remove(entity);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public int countNews(SearchCriteria searchCriteria) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		int result = 0;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			String jpql = buildQuery(searchCriteria);
			Query query = manager.createQuery(jpql);
			insertSearchCriteria(query, searchCriteria);
			result = query.getResultList().size();
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return result;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int pageId,
			int newsOnPage) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		List<News> news = new ArrayList<News>();
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			String jpql = buildQuery(searchCriteria);
			Query query = manager.createQuery(jpql);
			insertSearchCriteria(query, searchCriteria);
			int startIndex;
			startIndex = (pageId - 1) * newsOnPage;
			query.setFirstResult(startIndex);
			query.setMaxResults(newsOnPage);
			news = query.getResultList();
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return news;
	}

	private void insertSearchCriteria(Query query, SearchCriteria searchCriteria) {
		if (searchCriteria.getTagIds() != null) {
			query.setParameter("tagIds", searchCriteria.getTagIds());
		}
		if (searchCriteria.getAuthorId() != null) {
			query.setParameter("authorId", searchCriteria.getAuthorId());
		}
	}

	private String buildQuery(SearchCriteria searchCriteria) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT n FROM News n ");
		if (searchCriteria.getTagIds() != null) {
			jpql.append("JOIN n.tags as t ");
		}
		jpql.append("WHERE ");
		if (searchCriteria.getTagIds() != null) {
			jpql.append(" (t.id IN ?tagIds ) ");
		} else {
			jpql.append(" 1=1 ");
		}
		jpql.append(" AND");
		if (searchCriteria.getAuthorId() != null) {
			jpql.append(" (n.author.id = :authorId) ");
		} else {
			jpql.append(" 1=1 ");
		}
		jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
		return jpql.toString();
	}
}