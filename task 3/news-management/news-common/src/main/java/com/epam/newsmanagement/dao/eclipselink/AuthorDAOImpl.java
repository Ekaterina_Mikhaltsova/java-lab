package com.epam.newsmanagement.dao.eclipselink;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;

import static com.epam.newsmanagement.utils.JPADAOUtil.*;

/**
 * Class implementation of {@link AuthorDAO} interface.
 * <p>
 * Implements all operations on AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorDAOImpl implements AuthorDAO {

	private EntityManagerFactory managerFactory;

	public AuthorDAOImpl() {
		managerFactory = JPAEntityManager.getEntityManagerFactory();
	}

	@Override
	public void add(Author author) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			author.setId(null);
			manager.persist(author);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public void expired(Long authorId) throws DAOException {
		try {
			Author author = findById(authorId);
			author.setExpired(new Date(System.currentTimeMillis()));
			update(author);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Author findById(Long authorId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction = null;
		Author author = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			author = manager.find(Author.class, authorId);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return author;
	}

	@Override
	public List<Author> findAll() throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		List<Author> authors = new ArrayList<Author>();
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Query query = manager
					.createQuery("SELECT a FROM Author a ORDER BY a.name");
			authors = query.getResultList();
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
		return authors;
	}

	@Override
	public void update(Author author) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction = null;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Author tempAuthor = manager.find(Author.class, author.getId());
			tempAuthor.setName(author.getName());
			tempAuthor.setExpired(author.getExpired());
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}
}
