package com.epam.newsmanagement.dao.eclipselink;

import javax.persistence.*;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.JPAEntityManager;

import static com.epam.newsmanagement.utils.JPADAOUtil.*;

/**
 * Class implementation of {@link CommentDAO} interface.
 * <p>
 * Implements all operations on COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentDAOImpl implements CommentDAO {

	private EntityManagerFactory managerFactory;

	public CommentDAOImpl() {
		managerFactory = JPAEntityManager.getEntityManagerFactory();
	}

	@Override
	public void add(Comment comment) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			comment.setId(null);
			manager.persist(comment);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}

	@Override
	public void remove(Long commentId) throws DAOException {
		EntityManager manager = null;
		EntityTransaction transaction;
		try {
			manager = managerFactory.createEntityManager();
			transaction = manager.getTransaction();
			transaction.begin();
			Comment entity = manager.find(Comment.class, commentId);
			entity.setNewsId(null);
			manager.remove(entity);
			transaction.commit();
		} catch (PersistenceException e) {
			throw new DAOException(e);
		} finally {
			closeManager(manager);
		}
	}
}
