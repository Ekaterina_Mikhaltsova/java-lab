package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Class represents Comment model.
 *
 * @author Kate
 */
@Entity
@Table(name = "COMMENTS")
public class Comment implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Comment id.
	 */
	@Id
	@GeneratedValue(generator = "comments_seq")
	@SequenceGenerator(name = "comments_seq", sequenceName = "COMMENTS_SEQ", allocationSize = 1)
	@Column(name = "COMMENT_ID", nullable = false, unique = true)
	private Long id;

	@PrimaryKeyJoinColumn()
	@Column(name = "C_NEWS_ID", nullable = false, precision = 20)
	private Long newsId;

	/**
	 * Comment text.
	 */
	@Size(min = 1, max = 100, message = "{Size}")
	@Column(name = "COMMENT_TEXT")
	private String text;

	/**
	 * Comment creation date.
	 */
	@Column(name = "CREATION_DATE")
	private Date creationDate;

	/**
	 * Constructs {@link Comment} object with default field's values.
	 */
	public Comment() {
	}

	/**
	 * Constructs {@link Comment} object with given field's values.
	 */
	public Comment(Long id, Long newsId, String text, Date creationDate) {
		super();
		this.id = id;
		this.newsId = newsId;
		this.text = text;
		this.creationDate = creationDate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comment other = (Comment) obj;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Comment [id=" + id + ", newsId=" + newsId + ", text=" + text
				+ ", creationDate=" + creationDate + "]";
	}	
}
