package com.epam.newsmanagement.dao.hibernate;

import java.sql.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.epam.newsmanagement.dao.AuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;

/**
 * Class implementation of {@link AuthorDAO} interface.
 * <p>
 * Implements all operations on AUTHORS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class AuthorDAOImpl implements AuthorDAO {

	@Override
	public void add(Author author) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.save(author);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	@Override
	public void update(Author author) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(author);
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
	}

	@Override
	public void expired(Long authorId) throws DAOException {
		try {
			Author author = findById(authorId);
			author.setExpired(new Date(System.currentTimeMillis()));
			update(author);
		} catch (Exception e) {
			throw new DAOException(e);
		}
	}

	@Override
	public List<Author> findAll() throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<Author> authors;
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			authors = session.createCriteria(Author.class).list();
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return authors;
	}

	@Override
	public Author findById(Long authorId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Author author = null;
		try {
			author = (Author) session.get(Author.class, authorId);
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return author;
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}
}
