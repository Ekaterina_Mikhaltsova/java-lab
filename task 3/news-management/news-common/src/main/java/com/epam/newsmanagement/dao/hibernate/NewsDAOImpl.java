package com.epam.newsmanagement.dao.hibernate;

import java.sql.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.epam.newsmanagement.dao.NewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;
import com.epam.newsmanagement.utils.SearchCriteria;

/**
 * Class implementation of {@link NewsDAO} interface.
 * <p>
 * Implements all operations on NEWS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class NewsDAOImpl implements NewsDAO {

	@Override
	public Long add(News news) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try{
            transaction = session.beginTransaction();
            news.setModificationDate(new Date(System.currentTimeMillis()));
            session.save(news);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
        return news.getId();
	}

	@Override
	public News findById(Long newsId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        News news = null;
        try {
            news = (News) session.get(News.class, newsId);
            news.getComments().size();
            news.getTags().size();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
        return news;
	}

	@Override
	public void update(News news) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            news.setModificationDate(new Date(System.currentTimeMillis()));
            session.update(news);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}

	@Override
	public void remove(Long newsId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction;
        try {
            transaction = session.beginTransaction();
            News news = (News) session.load(News.class, newsId);
            news.setAuthor(null);
            news.setTags(null);
            session.delete(news);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}

	@Override
	public int countNews(SearchCriteria searchCriteria) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction ;
        int result = 0;
        try{
            transaction = session.beginTransaction();
            String hql = buildQuery(searchCriteria);
            Query query = session.createQuery(hql);
            insertSearchCriteria(query, searchCriteria);
            result = query.list().size();
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
        return result;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int pageId,
			int newsOnPage) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction;
		List<News> news = null;
		try {
			transaction = session.beginTransaction();
			String hql = buildQuery(searchCriteria);
			Query query = session.createQuery(hql);
			insertSearchCriteria(query, searchCriteria);
			int startIndex = (pageId - 1) * newsOnPage;
			query.setFirstResult(startIndex);
			query.setMaxResults(newsOnPage);
			news = query.list();
			if (news != null) {
				for (News temp : news) {
					temp.getComments().size();
					temp.getTags().size();
				}
			}
			if (news.size() == 0)
				news = null;
			transaction.commit();
		} catch (Exception e) {
			throw new DAOException(e);
		} finally {
			closeSession(session);
		}
		return news;
	}

	private void insertSearchCriteria(Query query, SearchCriteria searchCriteria) {
		if (searchCriteria.getTagIds() != null) {
			query.setParameterList("tagIds", searchCriteria.getTagIds());
		}
		if (searchCriteria.getAuthorId() != null) {
			query.setParameter("authorId", searchCriteria.getAuthorId());
		}
	}

	private void closeSession(Session session) {
		if (session.isOpen()) {
			session.close();
		}
	}

	private String buildQuery(SearchCriteria searchCriteria) {
		StringBuilder jpql = new StringBuilder();
		jpql.append("SELECT DISTINCT n FROM News n ");
		if (searchCriteria.getTagIds() != null) {
			jpql.append("JOIN n.tags as t ");
		}
		jpql.append("WHERE ");
		if (searchCriteria.getTagIds() != null) {
			jpql.append(" (t.id IN (:tagIds) ) ");
		} else {
			jpql.append(" 1=1 ");
		}
		jpql.append(" AND");
		if (searchCriteria.getAuthorId() != null) {
			jpql.append(" (n.author.id = :authorId) ");
		} else {
			jpql.append(" 1=1 ");
		}
		jpql.append("ORDER BY size(n.comments) DESC , n.modificationDate DESC");
		return jpql.toString();
	}
}
