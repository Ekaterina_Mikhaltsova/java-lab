package com.epam.newsmanagement.dao.hibernate;

import org.hibernate.Session;
import org.hibernate.Transaction;
import com.epam.newsmanagement.dao.CommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.utils.HibernateUtil;

/**
 * Class implementation of {@link CommentDAO} interface.
 * <p>
 * Implements all operations on COMMENTS table.
 * </p>
 * 
 * @author Kate
 *
 */
public class CommentDAOImpl implements CommentDAO {

	@Override
	public void add(Comment comment) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try{
            transaction = session.beginTransaction();
            session.save(comment);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}

	@Override
	public void remove(Long commentId) throws DAOException {
		Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Comment comment = (Comment) session.load(Comment.class, commentId);
            comment.setNewsId(null);
            session.delete(comment);
            transaction.commit();
        }catch (Exception e){
            throw new DAOException(e);
        }finally {
            closeSession(session);
        }
	}
	
	private void closeSession(Session session){
        if (session.isOpen()) {
            session.close();
        }
    }
}
