drop table "AUTHORS" cascade constraints;
drop table "TAGS" cascade constraints;
drop table "COMMENTS" cascade constraints;
drop table "NEWS_TAGS" cascade constraints;
drop table "NEWS_AUTHORS" cascade constraints;
drop table "NEWS" cascade constraints;