<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="admin-page">
	<form:form name="addNews" method="POST" action="/news-admin/add-news" modelAttribute="news">
		<div class="edit-add">
			<div class="field">
				<label for="title"><spring:message code="title.message" />:</label>
				<form:textarea path="title" cols="120" rows="1" /><br>
				<form:errors path="title" cssClass="error" />
			</div><br>
			<div class="field">
				<label for="creationDate"><spring:message code="date.message" />:</label>
				<form:textarea path="creationDate" value="${news.creationDate}" cols="120" rows="1" readonly="true"/>
			</div><br>
			<div class="field">
				<label for="shortText"><spring:message code="short.message" />:</label>
				<form:textarea path="shortText" cols="120" rows="3"/><br>
				<form:errors path="shortText" cssClass="error" />
			</div><br>
			<div class="field">
				<label for="fullText"><spring:message code="full.message" />:</label>
				<form:textarea path="fullText" cols="120" rows="6"/><br>
				<form:errors path="fullText" cssClass="error" />
			</div><br>
		</div>
		<div class="news-selects">
			<form:select path="author.id" name="author">
				<c:forEach items="${authors}" var="author">
					<option selected="selected" value="${author.id}">
						<c:out value="${author.name}" />
					</option>
				</c:forEach>
			</form:select> 
			<form:select path="tags" multiple="true" id="multi-dropbox">
				<c:forEach items="${tags}" var="tag">
					<option value="${tag.id}">
						<c:out value="${tag.name}" />
					</option>
				</c:forEach>
			</form:select>  
			<input type="submit" value="<spring:message code="save.button" />" />
		</div>
	</form:form>
	<script type="text/javascript">
		$("#multi-dropbox").dropdownchecklist({
			emptyText : "<spring:message code="select.tags" />",
			width : 150
		});
	</script>
</div>