<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="admin-page">
	<form:form name="editNews" method="POST" action="/news-admin/edit-news/${news.id}" commandName="news">
		<div class="edit-add">
			<div class="field">
				<label for="title"><spring:message code="title.message" />:</label>
				<form:textarea path="title" value="${news.title}" cols="120" rows="1" /><br>
				<form:errors path="title" cssClass="error" />
			</div><br>
			<div class="field">
				<label for="creationDate"><spring:message code="date.message" />:</label>
				<form:textarea path="creationDate" value="${news.creationDate}" cols="120" rows="1" readonly="true"/>
			</div><br>
			<div class="field">
				<label for="shortText"><spring:message code="short.message" />:</label>
				<form:textarea path="shortText" value="${news.shortText}" cols="120" rows="3"/><br>
				<form:errors path="shortText" cssClass="error" />
			</div><br>
			<div class="field">
				<label for="fullText"><spring:message code="full.message" />:</label>
				<form:textarea path="fullText" value="${news.fullText}" cols="120" rows="6"/><br>
				<form:errors path="fullText" cssClass="error" />
			</div><br>
		</div>
		<div class="news-selects">
			<form:select path="author.id" name="author">
				<c:if test="${newsAuthor != null}">
					<option value="-1"><spring:message code="select.author" /></option>
				</c:if>
				<c:forEach items="${authors}" var="author">
					<c:if test="${newsAuthor != author.id}">
						<option value="${author.id}">
							<c:out value="${author.name}" />
						</option>
					</c:if>
					<c:if test="${newsAuthor == author.id}">
						<option selected="selected" value="${author.id}">
							<c:out value="${author.name}" />
						</option>
					</c:if>
				</c:forEach>
			</form:select> 
			<form:select path="tags" multiple="true" id="multi-dropbox" >				
				 <c:forEach items="${tags}" var="tag">
					<c:set var="contains" value="false" />
					<c:forEach var="item" items="${newsTags}">
						<c:if test="${item == tag.id}">
							<c:set var="contains" value="true" />
						</c:if>
					</c:forEach>
					<c:if test="${contains}">
						<form:option selected="true" value="${tag.id}">
							<c:out value="${tag.name}" />
						</form:option>
					</c:if>
					<c:if test="${!contains}">
						<option value="${tag.id}">
							<c:out value="${tag.name}" />
						</option>
					</c:if>
				</c:forEach>
			</form:select>
			<input type="hidden" name="newsId" value="${news.id}" /> 
			<input type="submit" value="<spring:message code="save.button" />" />
		</div>
	</form:form>	
	<script type="text/javascript">
		$("#multi-dropbox").dropdownchecklist({
			emptyText : "<spring:message code="select.tags" />",
			width : 150
		});
	</script>
</div>