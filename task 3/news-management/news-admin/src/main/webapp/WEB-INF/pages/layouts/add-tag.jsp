<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="admin-page">
	<c:forEach var="tag" items="${tags}">
		<form:form id="updateForm${tag.id}" action="/news-admin/update-tag" method="post" modelAttribute="tag">
			<strong><spring:message code="tag.message" />:</strong> 
			<form:input path="name" type="text" name="tagName" id="text${tag.id}" 
			value="${tag.name}" 
			size="30" disabled="true" /> 
			<a id="edit${tag.id}" href="javascript:{}" onclick="switchEnableInput(${tag.id})"><spring:message code="edit.button" /></a> 
			<a id="update${tag.id}" href="javascript:{}" onclick="updateFormLink(${tag.id})" class="hidden"><spring:message code="update.button" /></a> 
			<a id="delete${tag.id}" href="/news-admin/delete-tag/${tag.id}" class="hidden"><spring:message code="delete.button" /></a> 
			<a id="cancel${tag.id}" href="javascript:{}" onclick="switchDisableInput(${tag.id})" class="hidden"><spring:message code="cancel.button" /></a><br> 
			<input type="hidden" name="tagId" value="${tag.id}" />
		</form:form>
	</c:forEach>
	<form:form action="/news-admin/add-tag" name="addTag" method="post" modelAttribute="tag">
		<strong><spring:message code="add.tag.message" />:</strong> 
		<form:textarea path="name" cols="65" rows="1"></form:textarea><br>
		<form:errors path="name" cssClass="error" /><br>
		<input type="submit" value="<spring:message code="save.button" />">
	</form:form>
</div>