<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div align="center">
	<img src="./resources/img/error.jpg"><br>
	<h1><spring:message code="error.404.message" /></h1>
</div>