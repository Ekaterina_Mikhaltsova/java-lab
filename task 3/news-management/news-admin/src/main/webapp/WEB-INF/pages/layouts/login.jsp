<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div align="center">
	<c:url value="/login" var="loginUrl" />
	<form action="${loginUrl}" method="post">
		<c:if test="${param.error != null}">
			<spring:message code="invalid" />
		</c:if><br><br>
			<label for="username"><spring:message code="login.message" />:</label><br>
			<input type="text" id="username" name="username" /><br>
			<label for="password"><spring:message code="password.message" />:</label><br>
			<input type="password" id="password" name="password" /><br><br>
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<button type="submit" class="btn">
			<spring:message code="login.button" />
		</button>
	</form>
</div>