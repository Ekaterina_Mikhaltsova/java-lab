<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="admin-page">
	<c:set var="news" value="${news}" />
	<div class="news-title"><c:out value="${news.title}" /></div><br>
	<div class="simple">(<spring:message code="by.message" /><c:out value=" ${news.author.name}" />)</div>
	<div class="date">
		<fmt:formatDate pattern="dd/MM/yyyy" value="${news.modificationDate}" />
	</div><br><br>
	<div class="simple"><c:out value="${news.fullText}" /></div>
	<div class="comment-area">
		<c:forEach var="comment" items="${news.comments}">
			<form:form action="/news-admin/delete-comment">
				<div class="comment-date">
					<fmt:formatDate pattern="dd/MM/yyyy" value="${comment.creationDate}" />
				</div><br>
				<div class="comment-text">
					<input class="delete-comment-button" type="submit" value="x" />
					<c:out value="${comment.text}" /> 
					<input type="hidden" name="newsId" value="${news.id}" /> 
					<input type="hidden" name="commentId" value="${comment.id}" />
				</div><br><br>
			</form:form>
		</c:forEach>
		<form:form name="addComment" method="POST" action="/news-admin/add-comment" modelAttribute="comment">
			<form:textarea path="text" cols="65" rows="6"></form:textarea><br>
			<form:errors path="text" cssClass="error" /><br>
			<input type="hidden" name="newsId" value="${news.id}" /><br>
			<div align="right" class="post-button">
				<input type="submit" value="<spring:message code="post.comment.button" />" />
			</div>
		</form:form><br>
	</div>
</div>