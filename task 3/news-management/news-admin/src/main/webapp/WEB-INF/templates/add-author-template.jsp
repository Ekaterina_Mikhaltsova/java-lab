<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
<link rel="stylesheet"
	href="<c:url value="/resources/css/style.css"></c:url>" type="text/css" />
<script src="<c:url value="/resources/js/script.js" />"></script>
<title><tiles:getAsString name="title" /></title>
</head>

<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="logout-menu" />
	<tiles:insertAttribute name="locale" />
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>

</html>