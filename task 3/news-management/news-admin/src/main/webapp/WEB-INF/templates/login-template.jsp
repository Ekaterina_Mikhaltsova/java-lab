<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<html>

<head>
	<title><tiles:getAsString name="title" /></title>
	<link rel="stylesheet"
	href="<c:url value="/resources/css/style.css"></c:url>" type="text/css" />
</head>

<body>
	<tiles:insertAttribute name="header" />
	<tiles:insertAttribute name="locale" />
	<tiles:insertAttribute name="content" />
	<tiles:insertAttribute name="footer" />
</body>

</html>