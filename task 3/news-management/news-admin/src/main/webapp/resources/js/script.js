function switchEnableInput(inputId) {
    document.getElementById("text"+inputId).disabled = false;
    document.getElementById("edit"+inputId).style.display = "none";
    document.getElementById("update"+inputId).style.visibility = "visible";
    document.getElementById("delete"+inputId).style.visibility = "visible";
    document.getElementById("cancel"+inputId).style.visibility = "visible";
}

function switchDisableInput(inputId) {
    document.getElementById("text"+inputId).disabled = true;
    document.getElementById("edit"+inputId).style.display = "inline";
    document.getElementById("update"+inputId).style.visibility = "hidden";
    document.getElementById("delete"+inputId).style.visibility = "hidden";
    document.getElementById("cancel"+inputId).style.visibility = "hidden";
}

function updateFormLink(inputId){
    document.forms["updateForm"+inputId].submit();
}