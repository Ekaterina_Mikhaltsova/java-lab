package com.epam.newsmanagement.controller;

import java.sql.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.CommentService;
import com.epam.newsmanagement.service.NewsService;

@Controller
public class CommentController {

	@Autowired
	private CommentService commentService;
	
	@Autowired
	private NewsService newsService;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-comment", method = RequestMethod.POST)
	public String addComment(@Valid @ModelAttribute("comment") Comment comment,
			BindingResult result, RedirectAttributes attr, Model model,
			@RequestParam(value = "newsId", required = true) Long newsId)
			throws ServiceException {
		if (result.hasErrors()) {
			attr.addFlashAttribute(
					"org.springframework.validation.BindingResult.comment",
					result);
			attr.addFlashAttribute("comment", comment);
		} else {
			comment.setNewsId(newsId);
			comment.setCreationDate(new Date(System.currentTimeMillis()));
			commentService.add(comment);
		}
		return "redirect:/single-news/" + newsId;
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/delete-comment")
	public String deleteComment(
			@RequestParam(value = "newsId", required = false) Long newsId,
			@RequestParam(value = "commentId", required = false) Long commentId,
			Model model) throws ServiceException {
		commentService.remove(commentId);
		return "redirect:/single-news/" + newsId;
	}
}
