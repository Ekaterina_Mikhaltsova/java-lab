package com.epam.newsmanagement.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.AuthorService;
import com.epam.newsmanagement.service.NewsService;
import com.epam.newsmanagement.service.TagService;

@Controller
public class MenuController {

	@Autowired
	private TagService tagService;
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private NewsService newsService;

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-news-view", method = RequestMethod.GET)
	public String addNews(Model model) throws ServiceException {
		Date date = new Date(System.currentTimeMillis());
		List<Author> authors = authorService.findAll();
		List<Tag> tags = tagService.findAll();
		if (!model.containsAttribute("news")) {
			model.addAttribute("news", new News(0L, null, null, null, date,
					null, null, null, null));
		}
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		return "add-news-view";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-author-view")
	public String addAuthor(Model model, RedirectAttributes attr)
			throws ServiceException {
		List<Author> authors = authorService.findAll();
		model.addAttribute("authors", authors);
		if (!model.containsAttribute("author")) {
			model.addAttribute("author", new Author());
		}
		return "add-author-view";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/add-tag-view")
	public String addTag(Model model, RedirectAttributes attr)
			throws ServiceException {
		List<Tag> tags = tagService.findAll();
		model.addAttribute("tags", tags);
		if (!model.containsAttribute("tag")) {
			model.addAttribute("tag", new Tag());
		}
		return "add-tag-view";
	}

	@Secured("ROLE_ADMIN")
	@RequestMapping(value = "/edit-news-view/{newsId}", method = RequestMethod.GET)
	public String editNews(@PathVariable Long newsId, Model model,
			HttpSession session) throws ServiceException {
		News news = newsService.findById(newsId);
		List<Author> authors = authorService.findAll();
		List<Tag> tags = tagService.findAll();
		List<Long> newsTags = new ArrayList<Long>();
		for (Tag tag : news.getTags()) {
			newsTags.add(tag.getId());
		}
		session.setAttribute("newsTags", newsTags);
		session.setAttribute("newsAuthor", news.getAuthor().getId());
		if (!model.containsAttribute("news")) {
			model.addAttribute("news", news);
		}
//		model.addAttribute("news", news);
		model.addAttribute("authors", authors);
		model.addAttribute("tags", tags);
		return "edit-news-view";
	}
}
